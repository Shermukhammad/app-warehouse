package uz.pdp.appwarehouse.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AttachmentDto implements Serializable {
    private String originalFileName;
    private String contentType;
    private Long size;
    private byte[] content;
}