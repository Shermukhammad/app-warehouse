package uz.pdp.appwarehouse.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InputDto implements Serializable {
    private Long supplierId;
    private Long warehouseId;
    private Long currencyId;
    private String factureNumber;
}
