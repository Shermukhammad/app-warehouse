package uz.pdp.appwarehouse.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements Serializable {

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private String password;

    private Set<Long> warehouseIds;

    public Boolean active = true;
}