package uz.pdp.appwarehouse.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class OutputProductDto implements Serializable {

    private Double amount;

    private Double price;

    private Long productId;

    private Long outputId;

}