package uz.pdp.appwarehouse.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class OutputDto implements Serializable {
    private Long clientId;
    private Long warehouseId;
    private Long currencyId;
    private String factureNumber;
}
