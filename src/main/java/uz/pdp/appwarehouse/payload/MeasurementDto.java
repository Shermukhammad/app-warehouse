package uz.pdp.appwarehouse.payload;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class MeasurementDto extends AbsDto{
}
