package uz.pdp.appwarehouse.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InputProductDto implements Serializable {

    private Double amount;

    private Double price;

    private LocalDate expireDate;

    private Long productId;

    private Long inputId;

}
