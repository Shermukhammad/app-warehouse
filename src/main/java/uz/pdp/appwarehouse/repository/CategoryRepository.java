package uz.pdp.appwarehouse.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.pdp.appwarehouse.entity.Category;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query(value = "select cat from Category cat where cat.parentCategory is null")
    List<Category> findSuperParentCategories();

    @Query(value = "select cat from Category cat where cat.parentCategory is null and cat.active=true")
    List<Category> findActiveSuperParentCategories();

    @Query(value = "select cat from Category cat " +
            "where cat.parentCategory.id = :parentCategoryId")
    List<Category> findChildrenCategories(@Param("parentCategoryId") Long parentCategoryId);

    @Query(value = "select cat from Category cat " +
            "where cat.parentCategory.id = :parentCategoryId and cat.active=true ")
    List<Category> findActiveChildrenCategories(@Param("parentCategoryId") Long parentCategoryId);

    Page<Category> findAllByActiveIsTrue(Pageable pageable);

}