package uz.pdp.appwarehouse.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.pdp.appwarehouse.entity.Output;
import uz.pdp.appwarehouse.entity.OutputProduct;
import uz.pdp.appwarehouse.projections.DailyProductReport;

import java.time.LocalDate;

@Repository
public interface OutputProductRepository extends JpaRepository<OutputProduct, Long> {

    void deleteAllByOutput(Output output);

    Page<OutputProduct> findAllByOutput_Id(Long id, Pageable pageable);

    Page<OutputProduct> findAllByPriceGreaterThanEqualAndPriceLessThanEqual(Double fromPrice, Double toPrice, Pageable pageable);

    @Query(value = "select p.name as productName," +
            "       p.code as productCode," +
            "       sum(op.amount) as totalAmount," +
            "       m.name as measurementName," +
            "       op.price," +
            "       (sum(op.amount) * op.price) as totalPrice" +
            " from output_product op" +
            "         join output o on op.output_id = o.id" +
            "         join product p on op.product_id = p.id" +
            "         join measurement m on m.id = p.measurement_id" +
            " where date(o.date_time) = :date" +
            " group by p.name, p.code, m.name, op.price" +
            " order by totalPrice desc ", nativeQuery = true)
    Page<DailyProductReport> getProductReportByDate(@Param("date") LocalDate date, Pageable pageable);

}
