package uz.pdp.appwarehouse.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.appwarehouse.entity.Product;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findAllByCategory_Id(Long categoryId);

    List<Product> findAllByMeasurement_Id(Long measurementId);

    Page<Product> findAllByActiveIsTrue(Pageable pageable);

    List<Product> findAllByActiveTrueAndCategory_Id(Long categoryId);

    List<Product> findAllByActiveTrueAndMeasurement_Id(Long measurementId);


}
