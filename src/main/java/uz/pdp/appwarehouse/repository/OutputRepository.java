package uz.pdp.appwarehouse.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.appwarehouse.entity.Output;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface OutputRepository extends JpaRepository<Output, Long> {

    Page<Output> findAllByFactureNumber(String factureNumber, Pageable pageable);

    Output findByCode(String code);

    Page<Output> findAllByWarehouse_Id(Long warehouseId, Pageable pageable);

    Page<Output> findAllByClient_Id(Long clientId, Pageable pageable);

    Page<Output> findAllByCurrency_Id(Long currencyId, Pageable pageable);

    Page<Output> findAllByClient_IdAndWarehouse_Id(Long clientId, Long warehouseId, Pageable pageable);

    Page<Output> findAllByClient_IdAndCurrency_Id(Long clientId, Long currencyId, Pageable pageable);

    Page<Output> findAllByWarehouse_IdAndCurrency_Id(Long warehouseId, Long currencyId, Pageable pageable);

    List<Output> findAllByClient_IdAndWarehouse_IdAndCurrency_Id(Long clientId, Long warehouseId, Long currencyId);

    Page<Output> findAllByDateTimeGreaterThanEqualAndDateTimeLessThanEqual(LocalDateTime start, LocalDateTime end, Pageable pageable);

}
