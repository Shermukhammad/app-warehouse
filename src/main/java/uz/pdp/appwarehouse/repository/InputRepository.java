package uz.pdp.appwarehouse.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.appwarehouse.entity.Input;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface InputRepository extends JpaRepository<Input, Long> {

    Page<Input> findAllByFactureNumber(String factureNumber, Pageable pageable);

    Input findByCode(String code);

    Page<Input> findAllByWarehouse_Id(Long warehouseId, Pageable pageable);

    Page<Input> findAllBySupplier_Id(Long supplierId, Pageable pageable);

    Page<Input> findAllByCurrency_Id(Long currencyId, Pageable pageable);

    Page<Input> findAllBySupplier_IdAndWarehouse_Id(Long supplierId, Long warehouseId, Pageable pageable);

    Page<Input> findAllBySupplier_IdAndCurrency_Id(Long supplierId, Long currencyId, Pageable pageable);

    Page<Input> findAllByWarehouse_IdAndCurrency_Id(Long warehouseId, Long currencyId, Pageable pageable);

    List<Input> findAllBySupplier_IdAndWarehouse_IdAndCurrency_Id(Long supplierId, Long warehouseId, Long currencyId);

    Page<Input> findAllByDateTimeGreaterThanEqualAndDateTimeLessThanEqual(LocalDateTime start, LocalDateTime end, Pageable pageable);

}
