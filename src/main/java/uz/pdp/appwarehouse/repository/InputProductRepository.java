package uz.pdp.appwarehouse.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.pdp.appwarehouse.entity.Input;
import uz.pdp.appwarehouse.entity.InputProduct;
import uz.pdp.appwarehouse.projections.DailyProductReport;
import uz.pdp.appwarehouse.projections.ProductReportByExpireDate;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface InputProductRepository extends JpaRepository<InputProduct, Long> {

    Page<InputProduct> findAllByInput_Id(Long id, Pageable pageable);

    Page<InputProduct> findAllByPriceGreaterThanEqualAndPriceLessThanEqual(Double fromPrice, Double toPrice, Pageable pageable);

    List<InputProduct> findAllByExpireDate(LocalDate expireDate);

    Page<InputProduct> findAllByExpireDateGreaterThanEqualAndExpireDateLessThanEqual(LocalDate startDateTime, LocalDate endDate, Pageable pageable);

    @Query(value = "select p.name as productName," +
            "       p.code as productCode," +
            "       sum(ip.amount) as totalAmount," +
            "       m.name as measurementName," +
            "       ip.price," +
            "       (sum(ip.amount) * ip.price) as totalPrice" +
            " from input_product ip" +
            "         join input i on ip.input_id = i.id" +
            "         join product p on ip.product_id = p.id" +
            "         join measurement m on m.id = p.measurement_id" +
            " where date(i.date_time) = :date" +
            " group by p.name, p.code, m.name, ip.price", nativeQuery = true)
    Page<DailyProductReport> getProductReportByDate(@Param("date") LocalDate date, Pageable pageable);


    @Query(value = "select count(distinct p.name) as number_of_products_ex_date_in_next_3_days" +
                   " from input_product ip" +
                    " join product p on p.id = ip.product_id" +
                   " where ip.expire_date between current_date and current_date + 3",
                    nativeQuery = true)
    Long countProductsWithExpireDateInNext3Days();


    @Query(value = "select p.name as product," +
            "       ip.amount," +
            "       m.name as measurement," +
            "       ip.price," +
            "       c.name as currency," +
            "       w.name as warehouse," +
            "       s.name as supplier," +
            "       i.date_time as acceptedDateTime," +
            "       ip.expire_date as expireDate" +
            " from input_product ip" +
            " join product p on p.id = ip.product_id" +
            " join input i on i.id = ip.input_id" +
            " join warehouse w on i.warehouse_id = w.id" +
            " join supplier s on i.supplier_id = s.id" +
            " join measurement m on m.id = p.measurement_id" +
            " join currency c on i.currency_id = c.id" +
            " where ip.expire_date between :start and :end", nativeQuery = true)
    Page<ProductReportByExpireDate> getProductReportByExpireDateRange(@Param(value = "start") LocalDate start, @Param(value = "end") LocalDate end, Pageable pageable);

    Page<InputProduct> findAllByExpireDateGreaterThanEqualOrderByExpireDateDesc(LocalDate expireDate, Pageable pageable);

    void deleteAllByInput(Input input);

}