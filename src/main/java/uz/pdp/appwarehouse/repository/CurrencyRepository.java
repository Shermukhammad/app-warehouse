package uz.pdp.appwarehouse.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.appwarehouse.entity.Currency;


@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long> {

    Page<Currency> findAllByActiveIsTrue(Pageable pageable);

}