package uz.pdp.appwarehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Category;
import uz.pdp.appwarehouse.payload.CategoryDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;

import java.util.List;

@Service
public interface CategoryService {

    QueryCrudResponse addCategory(CategoryDto categoryDto);

    Category getCategoryById(Long id);

    Page<Category> getCategories(Integer pageNo, Integer pageSize);

    List<Category> getSuperParentCategories();

    List<Category> getActiveSuperParentCategories();

    List<Category> getChildrenCategories(Long parentCategoryId);

    List<Category> getActiveChildrenCategories(Long parentCategoryId);

    Page<Category> getActiveCategories(Integer pageNo, Integer pageSize);

    QueryCrudResponse updateCategory(Long id, CategoryDto categoryDto);

    QueryCrudResponse setActiveToInverse(Long id);

    QueryCrudResponse deleteCategory(Long id);

}