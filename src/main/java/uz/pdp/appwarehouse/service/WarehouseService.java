package uz.pdp.appwarehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Warehouse;
import uz.pdp.appwarehouse.payload.AbsDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;


@Service
public interface WarehouseService {

    QueryCrudResponse addWarehouse(AbsDto absDto);

    Warehouse getWarehouseById(Long Id);

    Page<Warehouse> getWarehouses(Integer pageNo, Integer pageSize);

    Page<Warehouse> getActiveWarehouses(Integer pageNo, Integer pageSize);

    QueryCrudResponse updateWarehouse(Long id, AbsDto absDto);

    QueryCrudResponse setActiveToInverse(Long id);

    QueryCrudResponse deleteWarehouse(Long id);

}