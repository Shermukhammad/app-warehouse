package uz.pdp.appwarehouse.service.serviceImpl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Warehouse;
import uz.pdp.appwarehouse.payload.AbsDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.repository.WarehouseRepository;
import uz.pdp.appwarehouse.service.WarehouseService;

import java.util.Optional;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    private final WarehouseRepository warehouseRepository;


    public WarehouseServiceImpl(WarehouseRepository warehouseRepository) {
        this.warehouseRepository = warehouseRepository;
    }


    @Override
    public Warehouse getWarehouseById(Long id) {
        Optional<Warehouse> byId = warehouseRepository.findById(id);
        return byId.orElseGet(Warehouse::new);
    }


    @Override
    public Page<Warehouse> getWarehouses(Integer pageNo, Integer pageSize) {

        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);

        return warehouseRepository.findAll(pageable);
    }


    @Override
    public Page<Warehouse> getActiveWarehouses(Integer pageNo, Integer pageSize) {

        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return warehouseRepository.findAllByActiveIsTrue(pageable);
    }


    @Override
    public QueryCrudResponse addWarehouse(AbsDto absDto) {
        Warehouse warehouse = new Warehouse();
        warehouse.setName(absDto.getName());
        warehouse.setActive(absDto.getActive());
        try {
            warehouseRepository.save(warehouse);
            return new QueryCrudResponse("Warehouse Added Successfully !", true);
        }
        catch (Exception e){
            return new QueryCrudResponse("This Warehouse already exists !", false);
        }
    }


    @Override
    public QueryCrudResponse updateWarehouse(Long id, AbsDto absDto) {

        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(id);
        if (!optionalWarehouse.isPresent())
            return new QueryCrudResponse("Warehouse not found !", false);

        Warehouse warehouse = optionalWarehouse.get();

        if (!warehouse.getActive())
            return new QueryCrudResponse("Warehouse is inactive !", false);

        String name = absDto.getName();

        if (!warehouse.getName().equals(name)){
            warehouse.setName(name);
            warehouse.setActive(absDto.getActive());
            try {
                warehouseRepository.save(warehouse);
                return new QueryCrudResponse("Warehouse Updated Successfully !", true);
            }
            catch (Exception e){
                return new QueryCrudResponse("This Warehouse already exists !", false);
            }
        }

        warehouse.setActive(absDto.getActive());
        warehouseRepository.save(warehouse);
        return new QueryCrudResponse("Warehouse Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse setActiveToInverse(Long id) {

        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(id);
        if (!optionalWarehouse.isPresent())
            return new QueryCrudResponse("Warehouse not found !", false);

        Warehouse warehouse = optionalWarehouse.get();

        Boolean active = warehouse.getActive();
        warehouse.setActive(!active);
        warehouseRepository.save(warehouse);

        if (active)
            return new QueryCrudResponse("Warehouse with id = " + id + " change to unActive", true);

        return new QueryCrudResponse("Warehouse with id = " + id + " change to active", true);
    }


    @Override
    public QueryCrudResponse deleteWarehouse(Long id) {

        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(id);
        if (!optionalWarehouse.isPresent())
            return new QueryCrudResponse("Warehouse Not Found !", false);

        Warehouse warehouse = optionalWarehouse.get();

        if (!warehouse.getActive())
            return new QueryCrudResponse("Warehouse is inactive !", false);

        warehouseRepository.delete(warehouse);
        return new QueryCrudResponse("Warehouse Deleted Successfully", true);
    }

}
