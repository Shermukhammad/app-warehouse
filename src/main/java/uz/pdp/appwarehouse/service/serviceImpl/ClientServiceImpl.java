package uz.pdp.appwarehouse.service.serviceImpl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Client;
import uz.pdp.appwarehouse.payload.ClientDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.repository.ClientRepository;
import uz.pdp.appwarehouse.service.ClientService;

import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;


    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }


    @Override
    public Client getClientById(Long id) {
        Optional<Client> byId = clientRepository.findById(id);
        return byId.orElseGet(Client::new);
    }


    @Override
    public Page<Client> getClients(Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return clientRepository.findAll(pageable);
    }


    @Override
    public Page<Client> getActiveClients(Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return clientRepository.findAllByActiveIsTrue(pageable);
    }


    @Override
    public QueryCrudResponse addClient(ClientDto clientDto) {

        Client client = new Client();
        client.setName(clientDto.getName());
        client.setActive(clientDto.getActive());
        client.setPhoneNumber(client.getPhoneNumber());
        try{
            clientRepository.save(client);
            return new QueryCrudResponse("Client Added Successfully !", true);
        }
        catch (Exception e) {
            return new QueryCrudResponse("This Phone Number already exists !", false);
        }
    }


    @Override
    public QueryCrudResponse updateClient(Long id, ClientDto clientDto) {

        Optional<Client> optionalClient = clientRepository.findById(id);
        if (!optionalClient.isPresent())
            return new QueryCrudResponse("Client Not Found !", false);

        Client client = optionalClient.get();
        if (!client.getActive())
            return new QueryCrudResponse("Client is inactive !", false);

        String phoneNumber = clientDto.getPhoneNumber();
        if (!client.getPhoneNumber().equals(phoneNumber)){
            client.setName(clientDto.getName());
            client.setActive(clientDto.getActive());
            client.setPhoneNumber(phoneNumber);
            try{
                clientRepository.save(client);
                return new QueryCrudResponse("Client Updated Successfully !", true);
            }
            catch (Exception e) {
                return new QueryCrudResponse("This Phone Number already exists !", false);
            }
        }

        client.setName(clientDto.getName());
        client.setActive(clientDto.getActive());
        return new QueryCrudResponse("Client Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse setActiveToInverse(Long id) {

        Optional<Client> optionalClient = clientRepository.findById(id);
        if (!optionalClient.isPresent())
            return new QueryCrudResponse("Client Not Found !", false);

        Client client = optionalClient.get();

        Boolean active = client.getActive();
        client.setActive(!active);

        clientRepository.save(client);

        if (active)
            return new QueryCrudResponse("Client with id = " + id + " change to unActive", true);

        return new QueryCrudResponse("Client with id = " + id + " change to active", true);
    }


    @Override
    public QueryCrudResponse deleteClient(Long id) {

        Optional<Client> optionalClient = clientRepository.findById(id);
        if (!optionalClient.isPresent())
            return new QueryCrudResponse("Client Not Found !", false);

        Client client = optionalClient.get();
        if (!client.getActive())
            return new QueryCrudResponse("Client is inactive !", false);

        clientRepository.delete(client);
        return new QueryCrudResponse("Client Deleted Successfully !", true);
    }

}
