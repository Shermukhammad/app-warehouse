package uz.pdp.appwarehouse.service.serviceImpl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Output;
import uz.pdp.appwarehouse.entity.OutputProduct;
import uz.pdp.appwarehouse.entity.Product;
import uz.pdp.appwarehouse.payload.OutputProductDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.projections.DailyProductReport;
import uz.pdp.appwarehouse.repository.OutputProductRepository;
import uz.pdp.appwarehouse.repository.OutputRepository;
import uz.pdp.appwarehouse.repository.ProductRepository;
import uz.pdp.appwarehouse.service.OutputProductService;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class OutputProductServiceImpl implements OutputProductService {

    private final OutputProductRepository outputProductRepository;
    private final OutputRepository outputRepository;
    private final ProductRepository productRepository;


    public OutputProductServiceImpl(OutputProductRepository outputProductRepository, OutputRepository outputRepository, ProductRepository productRepository) {
        this.outputProductRepository = outputProductRepository;
        this.outputRepository = outputRepository;
        this.productRepository = productRepository;
    }


    @Override
    public OutputProduct getOutputProduct(Long id) {
        Optional<OutputProduct> optOutputProduct = outputProductRepository.findById(id);
        return optOutputProduct.orElseGet(OutputProduct::new);
    }


    @Override
    public Page<OutputProduct> getOutputProductsByOutput(Long outputId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return outputProductRepository.findAllByOutput_Id(outputId, pageable);
    }


    @Override
    public Page<OutputProduct> getOutputProductsByPriceBetween(Double minPrice, Double maxPrice, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return outputProductRepository.findAllByPriceGreaterThanEqualAndPriceLessThanEqual(minPrice, maxPrice, pageable);
    }


    @Override
    public Page<DailyProductReport> getProductReportByDate(LocalDate date, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return outputProductRepository.getProductReportByDate(date, pageable);
    }


    @Override
    public QueryCrudResponse addOutputProduct(OutputProductDto outputProductDto) {

        Long outputId = outputProductDto.getOutputId();
        Optional<Output> optionalOutput = outputRepository.findById(outputId);
        if (!optionalOutput.isPresent())
            return new QueryCrudResponse("Output Not Found !", false);

        Long productId = outputProductDto.getProductId();
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if (!optionalProduct.isPresent())
            return new QueryCrudResponse("Product Not Found !", false);

        OutputProduct outputProduct = new OutputProduct();
        outputProduct.setOutput(optionalOutput.get());
        outputProduct.setProduct(optionalProduct.get());
        outputProduct.setPrice(outputProductDto.getPrice());
        outputProduct.setAmount(outputProductDto.getAmount());

        outputProductRepository.save(outputProduct);
        return new QueryCrudResponse("Output Product Added Successfully !", true);
    }


    @Override
    public QueryCrudResponse addAllOutputProducts(List<OutputProductDto> dtoList) {

        int count = 0;

        int size = dtoList.size();

        for (OutputProductDto outputProductDto : dtoList) {
            QueryCrudResponse queryCrudResponse =
                    addOutputProduct(outputProductDto);
            if (!queryCrudResponse.getStatus())
                count++;
        }

        if (count == 0)
            return new QueryCrudResponse("All output products" + " added successfully ", true);

        return new QueryCrudResponse((size - count) +
                " output products added successfully, " +
                count + " output products failed to add", true);
    }


    @Override
    public QueryCrudResponse updateOutputProduct(Long id, OutputProductDto outputProductDto) {

        Optional<OutputProduct> optionalOutputProduct = outputProductRepository.findById(id);
        if (!optionalOutputProduct.isPresent())
            return new QueryCrudResponse("Output Product Not Found !", false);

        OutputProduct outputProduct = optionalOutputProduct.get();
        Output output = outputProduct.getOutput();
        Long outputId = outputProductDto.getOutputId();

        if (!output.getId().equals(outputId)){
            Optional<Output> optionalOutput = outputRepository.findById(outputId);
            if (!optionalOutput.isPresent())
                return new QueryCrudResponse("Output Not Found !", false);
            output = optionalOutput.get();
        }

        Long productId = outputProductDto.getProductId();
        Product product = outputProduct.getProduct();

        if (!product.getId().equals(productId)){
            Optional<Product> optionalProduct = productRepository.findById(productId);
            if (!optionalProduct.isPresent())
                return new QueryCrudResponse("Product Not Found !", false);
            product = optionalProduct.get();
        }

        outputProduct.setOutput(output);
        outputProduct.setProduct(product);
        outputProduct.setPrice(outputProductDto.getPrice());
        outputProduct.setAmount(outputProductDto.getAmount());

        outputProductRepository.save(outputProduct);
        return new QueryCrudResponse("Output Product Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse deleteOutputProduct(Long id) {
        try{
            outputProductRepository.deleteById(id);
            return new QueryCrudResponse("OutputProduct Deleted Successfully !", true);
        }
        catch (EmptyResultDataAccessException e){
            return new QueryCrudResponse("OutputProduct Not Found !", false);
        }
    }


    @Override
    public QueryCrudResponse deleteAllByOutput(Long outputId) {

        Optional<Output> optOutput = outputRepository.findById(outputId);
        if (!optOutput.isPresent())
            return new QueryCrudResponse("Output with id = " + outputId + " not found", false);

        outputProductRepository.deleteAllByOutput(optOutput.get());

        return new QueryCrudResponse("Output products belong to " +
                "output with id = " + outputId +
                "deleted successfully", true);
    }

}