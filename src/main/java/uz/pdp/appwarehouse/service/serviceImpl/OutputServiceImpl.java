package uz.pdp.appwarehouse.service.serviceImpl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Client;
import uz.pdp.appwarehouse.entity.Currency;
import uz.pdp.appwarehouse.entity.Output;
import uz.pdp.appwarehouse.entity.Warehouse;
import uz.pdp.appwarehouse.payload.OutputDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.repository.ClientRepository;
import uz.pdp.appwarehouse.repository.CurrencyRepository;
import uz.pdp.appwarehouse.repository.OutputRepository;
import uz.pdp.appwarehouse.repository.WarehouseRepository;
import uz.pdp.appwarehouse.service.OutputService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OutputServiceImpl implements OutputService {

    private final OutputRepository outputRepository;
    private final ClientRepository clientRepository;
    private final WarehouseRepository warehouseRepository;
    private final CurrencyRepository currencyRepository;

    public OutputServiceImpl(OutputRepository outputRepository, ClientRepository clientRepository, WarehouseRepository warehouseRepository, CurrencyRepository currencyRepository) {
        this.outputRepository = outputRepository;
        this.clientRepository = clientRepository;
        this.warehouseRepository = warehouseRepository;
        this.currencyRepository = currencyRepository;
    }


    @Override
    public Output getOutput(Long id) {
        Optional<Output> optionalOutput = outputRepository.findById(id);
        return optionalOutput.orElseGet(Output::new);
    }


    @Override
    public Output getByOutputCode(String code) {
        return outputRepository.findByCode(code);
    }


    @Override
    public Page<Output> getByFactureNumber(String factureNumber, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return outputRepository.findAllByFactureNumber(factureNumber, pageable);
    }


    @Override
    public Page<Output> getOutputsByWarehouse(Long warehouseId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return outputRepository.findAllByWarehouse_Id(warehouseId, pageable);
    }


    @Override
    public Page<Output> getOutputsByClient(Long clientId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return outputRepository.findAllByClient_Id(clientId, pageable);
    }


    @Override
    public Page<Output> getOutputsByCurrency(Long currencyId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return outputRepository.findAllByCurrency_Id(currencyId, pageable);
    }


    @Override
    public Page<Output> getOutputsByClientAndWarehouse(Long clientId, Long warehouseId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return outputRepository.findAllByClient_IdAndWarehouse_Id(clientId, warehouseId, pageable);
    }


    @Override
    public Page<Output> getOutputsByClientAndCurrency(Long clientId, Long currencyId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return outputRepository.findAllByClient_IdAndCurrency_Id(clientId, currencyId, pageable);
    }


    @Override
    public Page<Output> getOutputsByWarehouseAndCurrency(Long warehouseId, Long currencyId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return outputRepository.findAllByWarehouse_IdAndCurrency_Id(warehouseId, currencyId, pageable);
    }


    @Override
    public List<Output> getOutputsByClientAndWarehouseAndCurrency(Long clientId, Long warehouseId, Long currencyId) {
        return outputRepository.findAllByClient_IdAndWarehouse_IdAndCurrency_Id(clientId, warehouseId, currencyId);
    }


    @Override
    public Page<Output> getOutputsInBetweenTwoDate(LocalDate start, LocalDate end, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return outputRepository.findAllByDateTimeGreaterThanEqualAndDateTimeLessThanEqual(start.atStartOfDay(), end.atStartOfDay().plusDays(1).minusSeconds(1), pageable);
    }


    @Override
    public QueryCrudResponse addOutput(OutputDto outputDto) {

        Long clientId = outputDto.getClientId();
        Optional<Client> optionalClient = clientRepository.findById(clientId);
        if (!optionalClient.isPresent())
            return new QueryCrudResponse("Client Not Found !", false);

        Long warehouseId = outputDto.getWarehouseId();
        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(warehouseId);
        if (!optionalWarehouse.isPresent())
            return new QueryCrudResponse("Warehouse Not Found !", false);

        Long currencyId = outputDto.getCurrencyId();
        Optional<Currency> optionalCurrency = currencyRepository.findById(currencyId);
        if (!optionalCurrency.isPresent())
            return new QueryCrudResponse("Currency Not Found !", false);

        Output output = new Output();
        output.setFactureNumber(outputDto.getFactureNumber());
        output.setClient(optionalClient.get());
        output.setCurrency(optionalCurrency.get());
        output.setWarehouse(optionalWarehouse.get());
        output.setCode(UUID.randomUUID().toString());
        output.setDateTime(LocalDateTime.now());

        outputRepository.save(output);
        return new QueryCrudResponse("Output Added Successfully !", true);
    }


    @Override
    public QueryCrudResponse updateOutput(Long id, OutputDto outputDto) {

        Optional<Output> optionalOutput = outputRepository.findById(id);
        if (!optionalOutput.isPresent())
            return new QueryCrudResponse("Output Not Found !", false);

        Output output = optionalOutput.get();
        Warehouse warehouse = output.getWarehouse();

        Long warehouseId = outputDto.getWarehouseId();
        if (!warehouse.getId().equals(warehouseId)){
            Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(warehouseId);
            if (!optionalWarehouse.isPresent())
                return new QueryCrudResponse("Warehouse Not Found !", false);
            warehouse = optionalWarehouse.get();
        }

        Currency currency = output.getCurrency();
        Long currencyId = outputDto.getCurrencyId();

        if (!currency.getId().equals(currencyId)){
            Optional<Currency> optionalCurrency = currencyRepository.findById(currencyId);
            if (!optionalCurrency.isPresent())
                return new QueryCrudResponse("Currency Not Found !", false);
            currency = optionalCurrency.get();
        }

        Client client = output.getClient();
        Long clientId = outputDto.getClientId();

        if (!client.getId().equals(clientId)){
            Optional<Client> optionalClient = clientRepository.findById(clientId);
            if (!optionalClient.isPresent())
                return new QueryCrudResponse("Client Not Found !", false);
            client = optionalClient.get();
        }

        output.setFactureNumber(outputDto.getFactureNumber());
        output.setWarehouse(warehouse);
        output.setCurrency(currency);
        output.setClient(client);

        outputRepository.save(output);
        return new QueryCrudResponse("Output Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse deleteOutput(Long id) {
        try{
            outputRepository.deleteById(id);
            return new QueryCrudResponse("Output Deleted Successfully !", true);
        }
        catch (EmptyResultDataAccessException e){
            return new QueryCrudResponse("Output Not Found !", false);
        }
    }

}
