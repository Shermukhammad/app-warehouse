package uz.pdp.appwarehouse.service.serviceImpl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Currency;
import uz.pdp.appwarehouse.payload.AbsDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.repository.CurrencyRepository;
import uz.pdp.appwarehouse.service.CurrencyService;

import java.util.Optional;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;

    public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }


    @Override
    public Currency getCurrencyById(Long id) {
        Optional<Currency> byId = currencyRepository.findById(id);
        return byId.orElseGet(Currency::new);
    }


    @Override
    public Page<Currency> getCurrencies(Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return currencyRepository.findAll(pageable);
    }


    @Override
    public Page<Currency> getActiveCurrencies(Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return currencyRepository.findAllByActiveIsTrue(pageable);
    }


    @Override
    public QueryCrudResponse addCurrency(AbsDto absDto) {
        Currency currency = new Currency();
        currency.setName(absDto.getName());
        currency.setActive(absDto.getActive());
        try {
            currencyRepository.save(currency);
            return new QueryCrudResponse("Currency Added Successfully !", true);
        }
        catch (Exception e){
            return new QueryCrudResponse("This Currency already exists !", false);
        }
    }


    @Override
    public QueryCrudResponse updateCurrency(Long id, AbsDto absDto) {

        Optional<Currency> optionalCurrency = currencyRepository.findById(id);
        if (!optionalCurrency.isPresent())
            return new QueryCrudResponse("Currency not found !", false);

        Currency currency = optionalCurrency.get();
        if (!currency.getActive())
            return new QueryCrudResponse("Currency is inactive !", false);

        String name = absDto.getName();

        if (!currency.getName().equals(name)){
            currency.setName(name);
            currency.setActive(absDto.getActive());
            try{
                currencyRepository.save(currency);
                return new QueryCrudResponse("Currency Updated Successfully !", true);
            }
            catch (Exception e){
                return new QueryCrudResponse("This Currency already exists !", false);
            }
        }

        currency.setActive(absDto.getActive());
        currencyRepository.save(currency);
        return new QueryCrudResponse("Currency Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse setActiveToInverse(Long id) {

        Optional<Currency> optionalCurrency = currencyRepository.findById(id);
        if (!optionalCurrency.isPresent())
            return new QueryCrudResponse("Currency not found !", false);

        Currency currency = optionalCurrency.get();

        Boolean active = currency.getActive();
        currency.setActive(!active);

        currencyRepository.save(currency);

        if (active)
            return new QueryCrudResponse("Currency with id = " + id + " change to unActive", true);

        return new QueryCrudResponse("Currency with id = " + id + " change to active", true);
    }


    @Override
    public QueryCrudResponse deleteCurrency(Long id) {

        Optional<Currency> optionalCurrency = currencyRepository.findById(id);
        if (!optionalCurrency.isPresent())
            return new QueryCrudResponse("Currency Not Found !", false);

        Currency currency = optionalCurrency.get();
        if (!currency.getActive())
            return new QueryCrudResponse("Currency is inactive !", false);

        currencyRepository.delete(currency);
        return new QueryCrudResponse("Currency Deleted Successfully", true);
    }

}
