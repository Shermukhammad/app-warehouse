package uz.pdp.appwarehouse.service.serviceImpl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Category;
import uz.pdp.appwarehouse.payload.CategoryDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.repository.CategoryRepository;
import uz.pdp.appwarehouse.service.CategoryService;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public Category getCategoryById(Long id){
        Optional<Category> byId = categoryRepository.findById(id);
        return byId.orElseGet(Category::new);
    }


    @Override
    public Page<Category> getCategories(Integer pageNo, Integer pageSize){

        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return categoryRepository.findAll(pageable);
    }


    @Override
    public List<Category> getSuperParentCategories() {
        return categoryRepository.findSuperParentCategories();
    }


    @Override
    public List<Category> getActiveSuperParentCategories() {
        return categoryRepository.findActiveSuperParentCategories();
    }


    @Override
    public List<Category> getChildrenCategories(Long parentCategoryId) {
        return categoryRepository.findChildrenCategories(parentCategoryId);
    }


    @Override
    public List<Category> getActiveChildrenCategories(Long parentCategoryId) {
        return categoryRepository.findActiveChildrenCategories(parentCategoryId);
    }


    @Override
    public Page<Category> getActiveCategories(Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return categoryRepository.findAllByActiveIsTrue(pageable);
    }


    @Override
    public QueryCrudResponse addCategory(CategoryDto categoryDto){

        Long parentCategoryId = categoryDto.getParentCategoryId();
        Category parentCategory = null;
        if (parentCategoryId != null){
            Optional<Category> optionalParentCategory = categoryRepository.findById(parentCategoryId);
            if (!optionalParentCategory.isPresent())
                return new QueryCrudResponse("Parent Category Not Found !", false);
            parentCategory = optionalParentCategory.get();
        }

        Category category = new Category();
        category.setName(categoryDto.getName());
        category.setActive(categoryDto.getActive());
        if (parentCategory != null)
            category.setParentCategory(parentCategory);
        try{
            categoryRepository.save(category);
            return new QueryCrudResponse("Category Added Successfully !", true);
        }
        catch (Exception e){
            return new QueryCrudResponse("This Category already exists !", false);
        }
    }


    @Override
    public QueryCrudResponse updateCategory(Long id, CategoryDto categoryDto){

        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (!optionalCategory.isPresent())
            return new QueryCrudResponse("Category Not Found !", false);

        Category category = optionalCategory.get();
        if (!category.getActive())
            return new QueryCrudResponse("Category is inactive !", false);

        Category parentCategory = category.getParentCategory();

        boolean checkParentCategoryChange = false;

        Long parentCategoryId = categoryDto.getParentCategoryId();
        if (parentCategoryId != null && (parentCategory == null || !parentCategory.getId().equals(parentCategoryId))) {
            Optional<Category> optionalParentCategory = categoryRepository.findById(parentCategoryId);
            if (!optionalParentCategory.isPresent())
                return new QueryCrudResponse("Parent Category Not Found !", false);
            parentCategory = optionalParentCategory.get();
            checkParentCategoryChange = true;
        }

        String name = categoryDto.getName();
        if (!category.getName().equals(name)) {
            category.setName(name);
            category.setActive(categoryDto.getActive());
            if (checkParentCategoryChange)
                category.setParentCategory(parentCategory);
            try{
                categoryRepository.save(category);
                return new QueryCrudResponse("Category Updated Successfully !", true);
            }
            catch (Exception e){
                return new QueryCrudResponse("This Category already exists !", false);
            }
        }

        category.setActive(categoryDto.getActive());
        if (checkParentCategoryChange)
            category.setParentCategory(parentCategory);

        categoryRepository.save(category);
        return new QueryCrudResponse("Category Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse setActiveToInverse(Long id) {

        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (!optionalCategory.isPresent())
            return new QueryCrudResponse("Category Not Found !", false);

        Category category = optionalCategory.get();

        Boolean active = category.getActive();
        category.setActive(!active);

        categoryRepository.save(category);

        if (active)
            return new QueryCrudResponse("Category with id = " + id + " change to unActive", true);

        return new QueryCrudResponse("Category with id = " + id + " change to active", true);
    }


    @Override
    public QueryCrudResponse deleteCategory(Long id){

        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (!optionalCategory.isPresent())
            return new QueryCrudResponse("Category Not Found !", false);

        Category category = optionalCategory.get();
        if (!category.getActive())
            return new QueryCrudResponse("Category is inactive !", false);

        categoryRepository.delete(category);
        return new QueryCrudResponse("Category Deleted Successfully !", true);
    }

}