package uz.pdp.appwarehouse.service.serviceImpl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Supplier;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.payload.SupplierDto;
import uz.pdp.appwarehouse.repository.SupplierRepository;
import uz.pdp.appwarehouse.service.SupplierService;

import java.util.Optional;

@Service
public class SupplierServiceImpl implements SupplierService {

    private final SupplierRepository supplierRepository;


    public SupplierServiceImpl(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }


    @Override
    public Supplier getSupplierById(Long id) {
        Optional<Supplier> byId = supplierRepository.findById(id);
        return byId.orElseGet(Supplier::new);
    }


    @Override
    public Page<Supplier> getSuppliers(Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return supplierRepository.findAll(pageable);
    }


    @Override
    public Page<Supplier> getActiveSuppliers(Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return supplierRepository.findAllByActiveIsTrue(pageable);
    }


    @Override
    public QueryCrudResponse addSupplier(SupplierDto supplierDto) {
        Supplier supplier = new Supplier();
        supplier.setName(supplierDto.getName());
        supplier.setActive(supplierDto.getActive());
        supplier.setPhoneNumber(supplierDto.getPhoneNumber());
        try{
            supplierRepository.save(supplier);
            return new QueryCrudResponse("Supplier Added Successfully !", true);
        }
        catch (Exception e){
            return new QueryCrudResponse("This Phone Number already exists !", false);
        }
    }


    @Override
    public QueryCrudResponse updateSupplier(Long id, SupplierDto supplierDto) {
        Optional<Supplier> optionalSupplier = supplierRepository.findById(id);
        if (!optionalSupplier.isPresent())
            return new QueryCrudResponse("Supplier Not Found !", false);

        Supplier supplier = optionalSupplier.get();
        if (!supplier.getActive())
            return new QueryCrudResponse("Supplier is inactive !", false);

        String phoneNumber = supplierDto.getPhoneNumber();
        if (!supplier.getPhoneNumber().equals(phoneNumber)){
            supplier.setName(supplierDto.getName());
            supplier.setActive(supplierDto.getActive());
            supplier.setPhoneNumber(phoneNumber);
            try{
                supplierRepository.save(supplier);
                return new QueryCrudResponse("Supplier Updated Successfully !", true);
            }
            catch (Exception e){
                return new QueryCrudResponse("This Phone Number already exists !", false);
            }
        }

        supplier.setName(supplierDto.getName());
        supplier.setActive(supplierDto.getActive());
        return new QueryCrudResponse("Supplier Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse setActiveToInverse(Long id) {

        Optional<Supplier> optionalSupplier = supplierRepository.findById(id);
        if (!optionalSupplier.isPresent())
            return new QueryCrudResponse("Supplier Not Found !", false);

        Supplier supplier = optionalSupplier.get();

        Boolean active = supplier.getActive();
        supplier.setActive(!active);

        supplierRepository.save(supplier);

        if (active)
            return new QueryCrudResponse("Supplier with id = " + id + " change to unActive", true);

        return new QueryCrudResponse("Supplier with id = " + id + " change to active", true);
    }


    @Override
    public QueryCrudResponse deleteSupplier(Long id) {
        Optional<Supplier> optionalSupplier = supplierRepository.findById(id);
        if (!optionalSupplier.isPresent())
            return new QueryCrudResponse("Supplier Not Found !", false);

        Supplier supplier = optionalSupplier.get();
        if (!supplier.getActive())
            return new QueryCrudResponse("Supplier is inactive !", false);

        supplierRepository.delete(supplier);
        return new QueryCrudResponse("Supplier Deleted Successfully !", true);
    }

}