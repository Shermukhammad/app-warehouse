package uz.pdp.appwarehouse.service.serviceImpl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Measurement;
import uz.pdp.appwarehouse.payload.AbsDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.repository.MeasurementRepository;
import uz.pdp.appwarehouse.service.MeasurementService;

import java.util.Optional;


@Service
public class MeasurementServiceImpl implements MeasurementService {

    private final MeasurementRepository measurementRepository;


    public MeasurementServiceImpl(MeasurementRepository measurementRepository) {
        this.measurementRepository = measurementRepository;
    }


    @Override
    public Measurement getMeasurementById(Long id){
        Optional<Measurement> byId = measurementRepository.findById(id);
        return byId.orElseGet(Measurement::new);
    }


    @Override
    public Page<Measurement> getMeasurements(Integer pageNo, Integer pageSize){
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);

        return measurementRepository.findAll(pageable);
    }


    @Override
    public Page<Measurement> getActiveMeasurements(Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return measurementRepository.findAllByActiveIsTrue(pageable);
    }


    @Override
    public QueryCrudResponse addMeasurement(AbsDto absDto){
        Measurement measurement = new Measurement();
        measurement.setName(absDto.getName());
        measurement.setActive(absDto.getActive());
        try {
            measurementRepository.save(measurement);
            return new QueryCrudResponse("Measurement Added Successfully !", true);
        }
        catch (Exception e){
            return new QueryCrudResponse("This Measurement already exists !", false);
        }
    }


    @Override
    public QueryCrudResponse updateMeasurement(Long id, AbsDto absDto){

        Optional<Measurement> optionalMeasurement = measurementRepository.findById(id);
        if (!optionalMeasurement.isPresent())
            return new QueryCrudResponse("Measurement not found !", false);

        Measurement measurement = optionalMeasurement.get();
        if (!measurement.getActive())
            return new QueryCrudResponse("Measurement is inactive !", false);

        String name = absDto.getName();
        if (!measurement.getName().equals(name)){
            measurement.setName(name);
            measurement.setActive(absDto.getActive());
            try{
                measurementRepository.save(measurement);
                return new QueryCrudResponse("Measurement Updated Successfully !", true);
            }
            catch (Exception e){
                return new QueryCrudResponse("This Measurement already exists !", false);
            }
        }

        measurement.setActive(absDto.getActive());
        measurementRepository.save(measurement);
        return new QueryCrudResponse("Measurement Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse setActiveToInverse(Long id) {

        Optional<Measurement> optionalMeasurement = measurementRepository.findById(id);
        if (!optionalMeasurement.isPresent())
            return new QueryCrudResponse("Measurement not found !", false);

        Measurement measurement = optionalMeasurement.get();

        Boolean active = measurement.getActive();
        measurement.setActive(!active);

        measurementRepository.save(measurement);

        if (active)
            return new QueryCrudResponse("Measurement with id = " + id + " change to unActive", true);

        return new QueryCrudResponse("Measurement with id = " + id + " change to active", true);
    }


    @Override
    public QueryCrudResponse deleteMeasurement(Long id){

        Optional<Measurement> optionalMeasurement = measurementRepository.findById(id);
        if (!optionalMeasurement.isPresent())
            return new QueryCrudResponse("Measurement Not Found !", false);

        Measurement measurement = optionalMeasurement.get();

        if (!measurement.getActive())
            return new QueryCrudResponse("Measurement is inactive !", false);

        measurementRepository.delete(measurement);
        return new QueryCrudResponse("Measurement Deleted Successfully", true);
    }


}