package uz.pdp.appwarehouse.service.serviceImpl;

import javafx.util.Pair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.User;
import uz.pdp.appwarehouse.entity.Warehouse;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.payload.UserDto;
import uz.pdp.appwarehouse.payload.WarehousesSetDto;
import uz.pdp.appwarehouse.repository.UserRepository;
import uz.pdp.appwarehouse.repository.WarehouseRepository;
import uz.pdp.appwarehouse.service.UserService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;


@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final WarehouseRepository warehouseRepository;


    public UserServiceImpl(UserRepository userRepository, WarehouseRepository warehouseRepository) {
        this.userRepository = userRepository;
        this.warehouseRepository = warehouseRepository;
    }


    @Override
    public User getUserById(Long id) {
        Optional<User> byId = userRepository.findById(id);
        return byId.orElseGet(User::new);
    }


    @Override
    public Page<User> getUsers(Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return userRepository.findAll(pageable);
    }


    @Override
    public Page<User> getActiveUsers(Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return userRepository.findAllByActiveIsTrue(pageable);
    }


    @Override
    public QueryCrudResponse addUser(UserDto userDto) {

        String phoneNumber = userDto.getPhoneNumber();
        if (userRepository.existsByPhoneNumber(phoneNumber))
            return new QueryCrudResponse("This Phone Number already exists !", false);

        Pair<Long, Set<Warehouse>> longSetPair = assignWarehouses(userDto.getWarehouseIds());
        Long key = longSetPair.getKey();
        if (key != null)
            return new QueryCrudResponse("Warehouse with id = " + key + " not found !", false);

        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPhoneNumber(phoneNumber);
        user.setCode(UUID.randomUUID().toString());
        user.setPassword(userDto.getPassword());
        user.setWarehouseSet(longSetPair.getValue());
        user.setActive(userDto.getActive());

        userRepository.save(user);
        return new QueryCrudResponse("User Added Successfully !", true);
    }


    @Override
    public QueryCrudResponse addWarehousesToUser(Long id, WarehousesSetDto warehousesSetDto) {

        Optional<User> byId = userRepository.findById(id);
        if (!byId.isPresent())
            return new QueryCrudResponse("User Not Found !", false);

        User user = byId.get();
        if (!user.getActive())
            return new QueryCrudResponse("User is inactive !", false);

        Pair<Long, Set<Warehouse>> longSetPair = assignWarehouses(warehousesSetDto.getWarehouseIds());
        Long key = longSetPair.getKey();
        if (key != null)
            return new QueryCrudResponse("Warehouse with id = " + key + " not found !", false);

        Set<Warehouse> warehouseSet = user.getWarehouseSet();
        warehouseSet.addAll(longSetPair.getValue());
        user.setWarehouseSet(warehouseSet);

        userRepository.save(user);
        return new QueryCrudResponse("Given WareHouses Successfully Added To User !", true);
    }


    @Override
    public QueryCrudResponse updateUser(Long id, UserDto userDto) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (!optionalUser.isPresent())
            return new QueryCrudResponse("User Not Found !", false);

        User user = optionalUser.get();
        if (!user.getActive())
            return new QueryCrudResponse("User is inactive !", false);

        String phoneNumber = userDto.getPhoneNumber();
        if (!user.getPhoneNumber().equals(phoneNumber))
            if (userRepository.existsByPhoneNumber(phoneNumber))
                return new QueryCrudResponse("This Phone Number already exists !", false);

        Pair<Long, Set<Warehouse>> longSetPair = assignWarehouses(userDto.getWarehouseIds());
        Long key = longSetPair.getKey();
        if (key != null)
            return new QueryCrudResponse("Warehouse with id = " + key + " not found !", false);

        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPhoneNumber(phoneNumber);
        user.setPassword(userDto.getPassword());
        user.setWarehouseSet(longSetPair.getValue());
        user.setActive(userDto.getActive());

        userRepository.save(user);
        return new QueryCrudResponse("User Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse deleteUser(Long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (!optionalUser.isPresent())
            return new QueryCrudResponse("User Not Found !", false);

        User user = optionalUser.get();
        if (!user.getActive())
            return new QueryCrudResponse("User is inactive !", false);

        userRepository.delete(user);
        return new QueryCrudResponse("User Deleted Successfully !", true);
    }


    @Override
    public QueryCrudResponse setActiveToInverse(Long id) {

        Optional<User> optionalUser = userRepository.findById(id);
        if (!optionalUser.isPresent())
            return new QueryCrudResponse("User Not Found !", false);

        User user = optionalUser.get();

        Boolean active = user.getActive();
        user.setActive(!active);

        userRepository.save(user);

        if (active)
            return new QueryCrudResponse("User with id = " + id + " change to unActive", true);

        return new QueryCrudResponse("User with id = " + id + " change to active", true);
    }


    @Override
    public QueryCrudResponse deleteWarehousesFromUser(Long id, WarehousesSetDto warehousesSetDto) {

        Optional<User> optionalUser = userRepository.findById(id);
        if (!optionalUser.isPresent())
            return new QueryCrudResponse("User Not Found !", false);

        User user = optionalUser.get();
        if (!user.getActive())
            return new QueryCrudResponse("User is inactive !", false);

        Pair<Long, Set<Warehouse>> pair = assignWarehouses(warehousesSetDto.getWarehouseIds());
        Long key = pair.getKey();
        if (key != null)
            return new QueryCrudResponse("Warehouse with id = " + key + " not found !", false);

        Set<Warehouse> warehouseSet = user.getWarehouseSet();
        warehouseSet.removeAll(pair.getValue());
        user.setWarehouseSet(warehouseSet);

        userRepository.save(user);
        return new QueryCrudResponse("Given Warehouses Successfully Deleted From User !", true);
    }


    private Pair<Long, Set<Warehouse>> assignWarehouses(Set<Long> warehouseIds) {
        Set<Warehouse> warehouses = new HashSet<>();

        for (Long warehouseId : warehouseIds) {
            Optional<Warehouse> warehouseOpt = warehouseRepository.findById(warehouseId);
            if (warehouseOpt.isPresent()){
                Warehouse warehouse = warehouseOpt.get();
                if (warehouse.getActive())
                    warehouses.add(warehouse);
            }
            else
                return new Pair<>(warehouseId, null);
        }

        return new Pair<>(null, warehouses);
    }

}