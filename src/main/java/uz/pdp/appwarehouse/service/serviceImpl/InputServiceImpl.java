package uz.pdp.appwarehouse.service.serviceImpl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Currency;
import uz.pdp.appwarehouse.entity.Input;
import uz.pdp.appwarehouse.entity.Supplier;
import uz.pdp.appwarehouse.entity.Warehouse;
import uz.pdp.appwarehouse.payload.InputDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.repository.CurrencyRepository;
import uz.pdp.appwarehouse.repository.InputRepository;
import uz.pdp.appwarehouse.repository.SupplierRepository;
import uz.pdp.appwarehouse.repository.WarehouseRepository;
import uz.pdp.appwarehouse.service.InputService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class InputServiceImpl implements InputService {

    private final InputRepository inputRepository;
    private final SupplierRepository supplierRepository;
    private final CurrencyRepository currencyRepository;
    private final WarehouseRepository warehouseRepository;


    public InputServiceImpl(InputRepository inputRepository, SupplierRepository supplierRepository, CurrencyRepository currencyRepository, WarehouseRepository warehouseRepository) {
        this.inputRepository = inputRepository;
        this.supplierRepository = supplierRepository;
        this.currencyRepository = currencyRepository;
        this.warehouseRepository = warehouseRepository;
    }


    @Override
    public Input getInput(Long id) {
        Optional<Input> optionalInput = inputRepository.findById(id);
        return optionalInput.orElseGet(Input::new);
    }


    @Override
    public Page<Input> getByFactureNumber(String factureNumber, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputRepository.findAllByFactureNumber(factureNumber, pageable);
    }


    @Override
    public Input getByInputCode(String code) {
        return inputRepository.findByCode(code);
    }


    @Override
    public Page<Input> getInputsByWarehouse(Long warehouseId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputRepository.findAllByWarehouse_Id(warehouseId, pageable);
    }


    @Override
    public Page<Input> getInputsBySupplier(Long supplierId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputRepository.findAllBySupplier_Id(supplierId, pageable);
    }


    @Override
    public Page<Input> getInputsByCurrency(Long currencyId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputRepository.findAllByCurrency_Id(currencyId, pageable);
    }


    @Override
    public Page<Input> getInputsBySupplierAndWarehouse(Long supplierId, Long warehouseId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputRepository.findAllBySupplier_IdAndWarehouse_Id(supplierId, warehouseId, pageable);
    }


    @Override
    public Page<Input> getInputsBySupplierAndCurrency(Long supplierId, Long currencyId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputRepository.findAllBySupplier_IdAndCurrency_Id(supplierId, currencyId, pageable);
    }


    @Override
    public Page<Input> getInputsByWarehouseAndCurrency(Long warehouseId, Long currencyId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputRepository.findAllByWarehouse_IdAndCurrency_Id(warehouseId, currencyId, pageable);
    }


    @Override
    public List<Input> getInputsBySupplierAndWarehouseAndCurrency(Long supplierId, Long warehouseId, Long currencyId) {
        return inputRepository.findAllBySupplier_IdAndWarehouse_IdAndCurrency_Id(supplierId, warehouseId, currencyId);
    }


    @Override
    public Page<Input> getInputsInBetweenTwoDate(LocalDate start, LocalDate end, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputRepository.findAllByDateTimeGreaterThanEqualAndDateTimeLessThanEqual(start.atStartOfDay(), end.atStartOfDay().plusDays(1).minusSeconds(1), pageable);
    }


    @Override
    public QueryCrudResponse addInput(InputDto inputDto) {

        Long supplierId = inputDto.getSupplierId();
        Optional<Supplier> optionalSupplier = supplierRepository.findById(supplierId);
        if (!optionalSupplier.isPresent())
            return new QueryCrudResponse("Supplier Not Found !", false);

        Long warehouseId = inputDto.getWarehouseId();
        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(warehouseId);
        if (!optionalWarehouse.isPresent())
            return new QueryCrudResponse("Warehouse Not Found !", false);

        Long currencyId = inputDto.getCurrencyId();
        Optional<Currency> optionalCurrency = currencyRepository.findById(currencyId);
        if (!optionalCurrency.isPresent())
            return new QueryCrudResponse("Currency Not Found !", false);

        Input input = new Input();
        input.setFactureNumber(inputDto.getFactureNumber());
        input.setWarehouse(optionalWarehouse.get());
        input.setCurrency(optionalCurrency.get());
        input.setSupplier(optionalSupplier.get());
        input.setCode(UUID.randomUUID().toString());
        input.setDateTime(LocalDateTime.now());

        inputRepository.save(input);
        return new QueryCrudResponse("Input Added Successfully !", true);
    }


    @Override
    public QueryCrudResponse updateInput(Long id, InputDto inputDto) {

        Optional<Input> optionalInput = inputRepository.findById(id);
        if (!optionalInput.isPresent())
            return new QueryCrudResponse("Input Not Found !", false);

        Input input = optionalInput.get();
        Warehouse warehouse = input.getWarehouse();

        Long warehouseId = inputDto.getWarehouseId();
        if (!warehouse.getId().equals(warehouseId)){
            Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(warehouseId);
            if (!optionalWarehouse.isPresent())
                return new QueryCrudResponse("Warehouse Not Found !", false);
            warehouse = optionalWarehouse.get();
        }

        Supplier supplier = input.getSupplier();
        Long supplierId = inputDto.getSupplierId();

        if (!supplier.getId().equals(supplierId)){
            Optional<Supplier> optionalSupplier = supplierRepository.findById(supplierId);
            if (!optionalSupplier.isPresent())
                return new QueryCrudResponse("Supplier Not Found !", false);
            supplier = optionalSupplier.get();
        }

        Currency currency = input.getCurrency();
        Long currencyId = inputDto.getCurrencyId();

        if (!currency.getId().equals(currencyId)){
            Optional<Currency> optionalCurrency = currencyRepository.findById(currencyId);
            if (!optionalCurrency.isPresent())
                return new QueryCrudResponse("Currency Not Found !", false);
            currency = optionalCurrency.get();
        }

        input.setFactureNumber(inputDto.getFactureNumber());
        input.setSupplier(supplier);
        input.setWarehouse(warehouse);
        input.setCurrency(currency);

        inputRepository.save(input);
        return new QueryCrudResponse("Input Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse deleteInput(Long id) {
        try{
            inputRepository.deleteById(id);
            return new QueryCrudResponse("Input Deleted Successfully !", true);
        }
        catch (EmptyResultDataAccessException e){
            return new QueryCrudResponse("Input Not Found !", false);
        }
    }

}