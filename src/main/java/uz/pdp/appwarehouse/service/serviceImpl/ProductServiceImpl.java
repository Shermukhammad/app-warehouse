package uz.pdp.appwarehouse.service.serviceImpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.appwarehouse.entity.*;
import uz.pdp.appwarehouse.payload.ProductDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.repository.*;
import uz.pdp.appwarehouse.service.ProductService;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final AttachmentRepository attachmentRepository;
    private final AttachmentContentRepository attachmentContentRepository;
    private final CategoryRepository categoryRepository;
    private final MeasurementRepository measurementRepository;


    public ProductServiceImpl(ProductRepository productRepository, AttachmentRepository attachmentRepository, AttachmentContentRepository attachmentContentRepository, CategoryRepository categoryRepository, MeasurementRepository measurementRepository) {
        this.productRepository = productRepository;
        this.attachmentRepository = attachmentRepository;
        this.attachmentContentRepository = attachmentContentRepository;
        this.categoryRepository = categoryRepository;
        this.measurementRepository = measurementRepository;
    }


    @Override
    public Product getProductById(Long id) {
        Optional<Product> byId = productRepository.findById(id);
        return byId.orElseGet(Product::new);
    }


    @Override
    public Page<Product> getProducts(Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return productRepository.findAll(pageable);
    }


    @Override
    public List<Product> getProductsByCategoryId(Long categoryId) {
        return productRepository.findAllByCategory_Id(categoryId);
    }


    @Override
    public List<Product> getProductsByMeasurementId(Long measurementId) {
        return productRepository.findAllByMeasurement_Id(measurementId);
    }


    @Override
    public Page<Product> getActiveProducts(Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return productRepository.findAllByActiveIsTrue(pageable);
    }


    @Override
    public List<Product> getActiveProductsByCategoryId(Long categoryId) {
        return productRepository.findAllByActiveTrueAndCategory_Id(categoryId);
    }


    @Override
    public List<Product> getActiveProductsByMeasurementId(Long measurementId) {
        return productRepository.findAllByActiveTrueAndMeasurement_Id(measurementId);
    }


    @SneakyThrows
    @Override
    public QueryCrudResponse addProduct(String model, MultipartFile file) {

        ObjectMapper mapper = new ObjectMapper();
        ProductDto productDto = mapper.readValue(model, ProductDto.class);

        Long categoryId = productDto.getCategoryId();
        Optional<Category> optionalCategory = categoryRepository.findById(categoryId);
        if (!optionalCategory.isPresent())
            return new QueryCrudResponse("Category Not Found !", false);

        Long measurementId = productDto.getMeasurementId();
        Optional<Measurement> optionalMeasurement = measurementRepository.findById(measurementId);
        if (!optionalMeasurement.isPresent())
            return new QueryCrudResponse("Measurement Not Found !", false);

        Attachment attachment = null;

        if (file != null){

            attachment = new Attachment();
            attachment.setSize(file.getSize());
            attachment.setOriginalFileName(file.getOriginalFilename());
            attachment.setContentType(file.getContentType());
            attachment = attachmentRepository.save(attachment);

            try {
                AttachmentContent attachmentContent = new AttachmentContent();
                attachmentContent.setContent(file.getBytes());
                attachmentContent.setAttachment(attachment);
                attachmentContentRepository.save(attachmentContent);
            }
            catch (IOException e) {
                return new QueryCrudResponse("Error in Reading File !", false);
            }

        }

        Product product = new Product();
        product.setName(productDto.getName());
        product.setActive(productDto.getActive());
        product.setCategory(optionalCategory.get());
        product.setMeasurement(optionalMeasurement.get());
        product.setCode(UUID.randomUUID().toString());
        if (attachment != null)
            product.setAttachment(attachment);

        productRepository.save(product);
        return new QueryCrudResponse("Product Added Successfully !", true);
    }


    @Override
    public QueryCrudResponse updateProduct(Long id, ProductDto productDto) {

        Optional<Product> optionalProduct = productRepository.findById(id);
        if (!optionalProduct.isPresent())
            return new QueryCrudResponse("Product Not Found !", false);

        Product product = optionalProduct.get();
        if (!product.getActive())
            return new QueryCrudResponse("Product is inactive !", false);

        Long categoryId = productDto.getCategoryId();
        Category category = product.getCategory();

        if (!category.getId().equals(categoryId)){
            Optional<Category> optionalCategory = categoryRepository.findById(categoryId);
            if (!optionalCategory.isPresent())
                return new QueryCrudResponse("Category Not Found !", false);
            category = optionalCategory.get();
        }

        Long measurementId = productDto.getMeasurementId();
        Measurement measurement = product.getMeasurement();
        if (!measurement.getId().equals(measurementId)){
            Optional<Measurement> optionalMeasurement = measurementRepository.findById(measurementId);
            if (!optionalMeasurement.isPresent())
                return new QueryCrudResponse("Measurement Not Found !", false);
            measurement = optionalMeasurement.get();
        }

        product.setName(productDto.getName());
        product.setActive(productDto.getActive());
        product.setCategory(category);
        product.setMeasurement(measurement);

        productRepository.save(product);
        return new QueryCrudResponse("Product Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse changeProductCategory(Long productId, Long categoryId) {

        Optional<Product> optionalProduct = productRepository.findById(productId);
        if (!optionalProduct.isPresent())
            return new QueryCrudResponse("Product Not Found !", false);

        Product product = optionalProduct.get();
        if (!product.getActive())
            return new QueryCrudResponse("Product is inactive !", false);

        if (!product.getCategory().getId().equals(categoryId)){
            Optional<Category> optionalCategory = categoryRepository.findById(categoryId);
            if (!optionalCategory.isPresent())
                return new QueryCrudResponse("Category Not Found !", false);
            product.setCategory(optionalCategory.get());
            productRepository.save(product);
        }

        return new QueryCrudResponse("Product Category Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse setActiveToInverse(Long id) {

        Optional<Product> optionalProduct = productRepository.findById(id);
        if (!optionalProduct.isPresent())
            return new QueryCrudResponse("Product Not Found !", false);

        Product product = optionalProduct.get();

        Boolean active = product.getActive();
        product.setActive(!active);

        productRepository.save(product);

        if (active)
            return new QueryCrudResponse("Product with id = " + id + " change to unActive", true);

        return new QueryCrudResponse("Product with id = " + id + " change to active", true);
    }


    @Override
    public QueryCrudResponse deleteProductById(Long id) {

        Optional<Product> optionalProduct = productRepository.findById(id);
        if (!optionalProduct.isPresent())
            return new QueryCrudResponse("Product Not Found !", false);

        Product product = optionalProduct.get();
        if (!product.getActive())
            return new QueryCrudResponse("Product is inactive !", false);

        Attachment attachment = product.getAttachment();

        productRepository.delete(product);

        if (attachment != null){
            Optional<AttachmentContent> optionalAttachmentContent = attachmentContentRepository.findByAttachment(attachment);
            optionalAttachmentContent.ifPresent(attachmentContentRepository::delete);
        }

        return new QueryCrudResponse("Product Deleted Successfully !", true);
    }

}
