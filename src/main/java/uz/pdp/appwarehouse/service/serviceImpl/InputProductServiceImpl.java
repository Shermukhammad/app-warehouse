package uz.pdp.appwarehouse.service.serviceImpl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Input;
import uz.pdp.appwarehouse.entity.InputProduct;
import uz.pdp.appwarehouse.entity.Product;
import uz.pdp.appwarehouse.payload.InputProductDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.projections.DailyProductReport;
import uz.pdp.appwarehouse.projections.ProductReportByExpireDate;
import uz.pdp.appwarehouse.repository.InputProductRepository;
import uz.pdp.appwarehouse.repository.InputRepository;
import uz.pdp.appwarehouse.repository.ProductRepository;
import uz.pdp.appwarehouse.service.InputProductService;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class InputProductServiceImpl implements InputProductService {

    private final InputProductRepository inputProductRepository;
    private final InputRepository inputRepository;
    private final ProductRepository productRepository;


    public InputProductServiceImpl(InputProductRepository inputProductRepository, InputRepository inputRepository, ProductRepository productRepository) {
        this.inputProductRepository = inputProductRepository;
        this.inputRepository = inputRepository;
        this.productRepository = productRepository;
    }


    @Override
    public InputProduct getInputProduct(Long id) {
        Optional<InputProduct> optionalInputProduct = inputProductRepository.findById(id);
        return optionalInputProduct.orElseGet(InputProduct::new);
    }


    @Override
    public Page<InputProduct> getInputProductsByInput(Long inputId, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputProductRepository.findAllByInput_Id(inputId, pageable);
    }


    @Override
    public Page<InputProduct> getInputProductsByPriceBetween(Double minPrice, Double maxPrice, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputProductRepository.findAllByPriceGreaterThanEqualAndPriceLessThanEqual(minPrice, maxPrice, pageable);
    }


    @Override
    public List<InputProduct> getInputProductsByExpireDate(LocalDate expireDate) {
        return inputProductRepository.findAllByExpireDate(expireDate);
    }


    @Override
    public Page<InputProduct> getInputProductsByExpireDateBetween(LocalDate start, LocalDate end, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputProductRepository.findAllByExpireDateGreaterThanEqualAndExpireDateLessThanEqual(start, end, pageable);
    }


    @Override
    public Page<DailyProductReport> getProductReportByDate(LocalDate date, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputProductRepository.getProductReportByDate(date, pageable);
    }


    @Override
    public Page<ProductReportByExpireDate> getProductReportByExpireDateRange(LocalDate start, LocalDate end, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputProductRepository.getProductReportByExpireDateRange(start, end, pageable);
    }


    @Override
    public Page<InputProduct> getInputProductsComingExpireDate(LocalDate date, Integer pageNo, Integer pageSize) {
        if (pageNo != 0)
            pageNo--;

        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return inputProductRepository.findAllByExpireDateGreaterThanEqualOrderByExpireDateDesc(date, pageable);
    }


    @Override
    public Long countProductsWithExpireDateInNext3Days() {
        return inputProductRepository.countProductsWithExpireDateInNext3Days();
    }


    @Override
    public QueryCrudResponse addInputProduct(InputProductDto inputProductDto) {

        Long productId = inputProductDto.getProductId();
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if (!optionalProduct.isPresent())
            return new QueryCrudResponse("Product Not Found !", false);

        Long inputId = inputProductDto.getInputId();
        Optional<Input> optionalInput = inputRepository.findById(inputId);
        if (!optionalInput.isPresent())
            return new QueryCrudResponse("Input Not Found !", false);

        InputProduct inputProduct = new InputProduct();
        inputProduct.setAmount(inputProductDto.getAmount());
        inputProduct.setPrice(inputProductDto.getPrice());
        inputProduct.setExpireDate(inputProductDto.getExpireDate());
        inputProduct.setProduct(optionalProduct.get());
        inputProduct.setInput(optionalInput.get());

        inputProductRepository.save(inputProduct);
        return new QueryCrudResponse("Input Product Added Successfully !", true);
    }


    @Override
    public QueryCrudResponse addAllInputProducts(List<InputProductDto> dtoList) {
        int count = 0;

        int size = dtoList.size();

        for (InputProductDto inputProductDto : dtoList) {
            QueryCrudResponse queryCrudResponse =
                    addInputProduct(inputProductDto);
            if (!queryCrudResponse.getStatus())
                count++;
        }

        if (count == 0)
            return new QueryCrudResponse("All input products" + " added successfully ", true);

        return new QueryCrudResponse((size - count) + " input products added successfully, " +
                count + " input products failed to add", true);
    }


    @Override
    public QueryCrudResponse updateInputProduct(Long id, InputProductDto inputProductDto) {

        Optional<InputProduct> byId = inputProductRepository.findById(id);
        if (!byId.isPresent())
            return new QueryCrudResponse("Input Product Not Found !", false);

        InputProduct inputProduct = byId.get();
        Product product = inputProduct.getProduct();

        Long productId = inputProductDto.getProductId();

        if (!product.getId().equals(productId)){
            Optional<Product> optionalProduct = productRepository.findById(productId);
            if (!optionalProduct.isPresent())
                return new QueryCrudResponse("Product Not Found !", false);
            product = optionalProduct.get();
        }

        Input input = inputProduct.getInput();
        Long inputId = inputProductDto.getInputId();

        if (!input.getId().equals(inputId)){
            Optional<Input> optionalInput = inputRepository.findById(inputId);
            if (!optionalInput.isPresent())
                return new QueryCrudResponse("Input Not Found !", false);
            input = optionalInput.get();
        }

        inputProduct.setAmount(inputProductDto.getAmount());
        inputProduct.setPrice(inputProductDto.getPrice());
        inputProduct.setExpireDate(inputProductDto.getExpireDate());
        inputProduct.setProduct(product);
        inputProduct.setInput(input);

        inputProductRepository.save(inputProduct);
        return new QueryCrudResponse("Input Product Updated Successfully !", true);
    }


    @Override
    public QueryCrudResponse deleteInputProduct(Long id){
        try{
            inputProductRepository.deleteById(id);
            return new QueryCrudResponse("Input Product Deleted Successfully !", true);
        }
        catch (EmptyResultDataAccessException e){
            return new QueryCrudResponse("Input Product Not Found !", false);
        }
    }


    @Override
    public QueryCrudResponse deleteAllByInput(Long inputId) {

        Optional<Input> optionalInput = inputRepository.findById(inputId);
        if (!optionalInput.isPresent())
            return new QueryCrudResponse("Input with id = "
                    + inputId + " not found", false);

        inputProductRepository.deleteAllByInput(optionalInput.get());
        return new QueryCrudResponse("Input products belong to " +
                "input with id = " + inputId +
                "deleted successfully", true);
    }


}
