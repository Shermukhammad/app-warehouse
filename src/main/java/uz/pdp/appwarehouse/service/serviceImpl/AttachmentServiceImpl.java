package uz.pdp.appwarehouse.service.serviceImpl;

import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.appwarehouse.entity.Attachment;
import uz.pdp.appwarehouse.entity.AttachmentContent;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.repository.AttachmentContentRepository;
import uz.pdp.appwarehouse.repository.AttachmentRepository;
import uz.pdp.appwarehouse.service.AttachmentService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    private final AttachmentRepository attachmentRepository;
    private final AttachmentContentRepository attachmentContentRepository;


    public AttachmentServiceImpl(AttachmentRepository attachmentRepository, AttachmentContentRepository attachmentContentRepository) {
        this.attachmentRepository = attachmentRepository;
        this.attachmentContentRepository = attachmentContentRepository;
    }


    @Override
    public Attachment getAttachmentById(Long id) {
        Optional<Attachment> byId = attachmentRepository.findById(id);
        return byId.orElseGet(Attachment::new);
    }


    @Override
    public QueryCrudResponse downloadAttachmentById(Long id, HttpServletResponse resp) {
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(id);

        if(optionalAttachment.isPresent()){

            Optional<AttachmentContent> optionalAttachmentContent = attachmentContentRepository.findByAttachment_Id(id);

            if(optionalAttachmentContent.isPresent()){
                Attachment attachment = optionalAttachment.get();

                resp.setHeader("Content-Disposition", "attachment; filename\""
                        +  attachment.getOriginalFileName() + "\"");

                resp.setContentType(attachment.getContentType());

                try {
                    FileCopyUtils.copy(optionalAttachmentContent.get().getContent(),
                            resp.getOutputStream());
                    return new QueryCrudResponse("File Downloaded Successfully !", true);
                }
                catch (IOException e) {
                    return new QueryCrudResponse("Error in Downloading File !", false);
                }
            }

            return new QueryCrudResponse("File is Empty !", false);
        }

        return new QueryCrudResponse("File Not Found !", false);
    }


    @Override
    public QueryCrudResponse addAttachment(MultipartFile file) {


        if (file != null){

            Attachment attachment = new Attachment();
            attachment.setSize(file.getSize());
            attachment.setOriginalFileName(file.getOriginalFilename());
            attachment.setContentType(file.getContentType());
            attachment = attachmentRepository.save(attachment);

            try {
                AttachmentContent attachmentContent = new AttachmentContent();
                attachmentContent.setContent(file.getBytes());
                attachmentContent.setAttachment(attachment);
                attachmentContentRepository.save(attachmentContent);
                return new QueryCrudResponse("File Added Successfully !", true);
            }
            catch (IOException e) {
                return new QueryCrudResponse("Error in Reading File !", false);
            }

        }

        return new QueryCrudResponse("No Files Are Sent !", false);
    }


    @Override
    public QueryCrudResponse updateAttachment(Long id, MultipartFile file) {

        Optional<Attachment> byId = attachmentRepository.findById(id);
        if (!byId.isPresent())
            return new QueryCrudResponse("File Not Found !", false);

        Attachment attachment = byId.get();
        if (file != null){

            attachment.setSize(file.getSize());
            attachment.setOriginalFileName(file.getOriginalFilename());
            attachment.setContentType(file.getContentType());

            try {
                Optional<AttachmentContent> optionalAttachmentContent = attachmentContentRepository.findByAttachment(attachment);
                AttachmentContent attachmentContent = optionalAttachmentContent.orElseGet(AttachmentContent::new);

                attachmentContent.setContent(file.getBytes());
                attachmentContent.setAttachment(attachment);
                attachmentContentRepository.save(attachmentContent);
                attachmentRepository.save(attachment);
                return new QueryCrudResponse("File Updated Successfully !", true);

            } catch (IOException e) {
                return new QueryCrudResponse("Error in Reading File !", false);
            }

        }

        return new QueryCrudResponse("No Files Are Sent !", false);
    }


    @Override
    public QueryCrudResponse deleteAttachmentById(Long id) {
        Optional<Attachment> byId = attachmentRepository.findById(id);
        if (!byId.isPresent())
            return new QueryCrudResponse("File Not Found !", false);

        attachmentRepository.deleteById(id);
        attachmentContentRepository.deleteByAttachment_Id(id);
        return new QueryCrudResponse("File with id = " + id + " deleted successfully !", true);
    }

}

