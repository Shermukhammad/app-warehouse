package uz.pdp.appwarehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Measurement;
import uz.pdp.appwarehouse.payload.AbsDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;


@Service
public interface MeasurementService {

    QueryCrudResponse addMeasurement(AbsDto absDto);

    Measurement getMeasurementById(Long id);

    Page<Measurement> getMeasurements(Integer pageNo, Integer pageSize);

    Page<Measurement> getActiveMeasurements(Integer pageNo, Integer pageSize);

    QueryCrudResponse updateMeasurement(Long id, AbsDto absDto);

    QueryCrudResponse setActiveToInverse(Long id);

    QueryCrudResponse deleteMeasurement(Long id);

}