package uz.pdp.appwarehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Output;
import uz.pdp.appwarehouse.payload.OutputDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;

import java.time.LocalDate;
import java.util.List;

@Service
public interface OutputService {

    QueryCrudResponse addOutput(OutputDto outputDto);

    Output getOutput(Long id);

    Output getByOutputCode(String code);

    Page<Output> getByFactureNumber(String factureNumber, Integer pageNo, Integer pageSize);

    Page<Output> getOutputsByWarehouse(Long warehouseId, Integer pageNo, Integer pageSize);

    Page<Output> getOutputsByClient(Long clientId, Integer pageNo, Integer pageSize);

    Page<Output> getOutputsByCurrency(Long currencyId, Integer pageNo, Integer pageSize);

    Page<Output> getOutputsByClientAndWarehouse(Long clientId, Long warehouseId, Integer pageNo, Integer pageSize);

    Page<Output> getOutputsByClientAndCurrency(Long clientId, Long currencyId, Integer pageNo, Integer pageSize);

    Page<Output> getOutputsByWarehouseAndCurrency(Long warehouseId, Long currencyId, Integer pageNo, Integer pageSize);

    List<Output> getOutputsByClientAndWarehouseAndCurrency(Long clientId, Long warehouseId, Long currencyId);

    Page<Output> getOutputsInBetweenTwoDate(LocalDate start, LocalDate end, Integer pageNo, Integer pageSize);

    QueryCrudResponse updateOutput(Long id, OutputDto outputDto);

    QueryCrudResponse deleteOutput(Long id);


}
