package uz.pdp.appwarehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Client;
import uz.pdp.appwarehouse.payload.ClientDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;


@Service
public interface ClientService {

    QueryCrudResponse addClient(ClientDto clientDto);

    Client getClientById(Long Id);

    Page<Client> getClients(Integer pageNo, Integer pageSize);

    Page<Client> getActiveClients(Integer pageNo, Integer pageSize);

    QueryCrudResponse updateClient(Long id, ClientDto clientDto);

    QueryCrudResponse setActiveToInverse(Long id);

    QueryCrudResponse deleteClient(Long id);

}
