package uz.pdp.appwarehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.OutputProduct;
import uz.pdp.appwarehouse.payload.OutputProductDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.projections.DailyProductReport;

import java.time.LocalDate;
import java.util.List;


@Service
public interface OutputProductService {

    QueryCrudResponse addOutputProduct(OutputProductDto outputProductDto);

    QueryCrudResponse addAllOutputProducts(List<OutputProductDto> dtoList);

    OutputProduct getOutputProduct(Long id);

    Page<OutputProduct> getOutputProductsByOutput(Long outputId, Integer pageNo, Integer pageSize);

    Page<OutputProduct> getOutputProductsByPriceBetween(Double minPrice, Double maxPrice, Integer pageNo, Integer pageSize);

    Page<DailyProductReport> getProductReportByDate(LocalDate date, Integer pageNo, Integer pageSize);

    QueryCrudResponse updateOutputProduct(Long id, OutputProductDto outputProductDto);

    QueryCrudResponse deleteOutputProduct(Long id);

    QueryCrudResponse deleteAllByOutput(Long outputId);

}