package uz.pdp.appwarehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.InputProduct;
import uz.pdp.appwarehouse.payload.InputProductDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.projections.DailyProductReport;
import uz.pdp.appwarehouse.projections.ProductReportByExpireDate;

import java.time.LocalDate;
import java.util.List;

@Service
public interface InputProductService {

    QueryCrudResponse addInputProduct(InputProductDto inputProductDto);

    QueryCrudResponse addAllInputProducts(List<InputProductDto> dtoList);

    InputProduct getInputProduct(Long id);

    Page<InputProduct> getInputProductsByInput(Long inputId, Integer pageNo, Integer pageSize);

    Page<InputProduct> getInputProductsByPriceBetween(Double minPrice, Double maxPrice, Integer pageNo, Integer pageSize);

    List<InputProduct> getInputProductsByExpireDate(LocalDate expireDate);

    Page<InputProduct> getInputProductsByExpireDateBetween(LocalDate start, LocalDate end, Integer pageNo, Integer pageSize);

    Page<InputProduct> getInputProductsComingExpireDate(LocalDate date, Integer pageNo, Integer pageSize);

    Long countProductsWithExpireDateInNext3Days();

    Page<ProductReportByExpireDate> getProductReportByExpireDateRange(LocalDate start, LocalDate end, Integer pageNo, Integer pageSize);

    Page<DailyProductReport> getProductReportByDate(LocalDate date, Integer pageNo, Integer pageSize);

    QueryCrudResponse updateInputProduct(Long id, InputProductDto inputProductDto);

    QueryCrudResponse deleteInputProduct(Long id);

    QueryCrudResponse deleteAllByInput(Long inputId);

}