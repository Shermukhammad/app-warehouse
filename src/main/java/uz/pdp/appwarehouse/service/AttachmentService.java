package uz.pdp.appwarehouse.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.appwarehouse.entity.Attachment;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;

import javax.servlet.http.HttpServletResponse;

@Service
public interface AttachmentService {

    QueryCrudResponse addAttachment(MultipartFile file);

    Attachment getAttachmentById(Long id);

    QueryCrudResponse downloadAttachmentById(Long id, HttpServletResponse resp);

    QueryCrudResponse updateAttachment(Long id, MultipartFile multipartFile);

    QueryCrudResponse deleteAttachmentById(Long id);
}
