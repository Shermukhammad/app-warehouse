package uz.pdp.appwarehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Supplier;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.payload.SupplierDto;


@Service
public interface SupplierService {

    QueryCrudResponse addSupplier(SupplierDto supplierDto);

    Supplier getSupplierById(Long Id);

    Page<Supplier> getSuppliers(Integer pageNo, Integer pageSize);

    Page<Supplier> getActiveSuppliers(Integer pageNo, Integer pageSize);

    QueryCrudResponse updateSupplier(Long id, SupplierDto supplierDto);

    QueryCrudResponse setActiveToInverse(Long id);

    QueryCrudResponse deleteSupplier(Long id);

}