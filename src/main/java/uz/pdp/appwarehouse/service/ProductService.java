package uz.pdp.appwarehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.appwarehouse.entity.Product;
import uz.pdp.appwarehouse.payload.ProductDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;

import java.util.List;

@Service
public interface ProductService {

    Product getProductById(Long id);

    Page<Product> getProducts(Integer pageNo, Integer pageSize);

    List<Product> getProductsByCategoryId(Long categoryId);

    List<Product> getProductsByMeasurementId(Long measurementId);

    Page<Product> getActiveProducts(Integer pageNo, Integer pageSize);

    List<Product> getActiveProductsByCategoryId(Long categoryId);

    List<Product> getActiveProductsByMeasurementId(Long measurementId);

    QueryCrudResponse addProduct(String model, MultipartFile file);

    QueryCrudResponse updateProduct(Long id, ProductDto productDto);

    QueryCrudResponse changeProductCategory(Long productId, Long categoryId);

    QueryCrudResponse setActiveToInverse(Long id);

    QueryCrudResponse deleteProductById(Long id);

}
