package uz.pdp.appwarehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Currency;
import uz.pdp.appwarehouse.payload.AbsDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;


@Service
public interface CurrencyService {

    QueryCrudResponse addCurrency(AbsDto absDto);

    Currency getCurrencyById(Long id);

    Page<Currency> getCurrencies(Integer pageNo, Integer pageSize);

    Page<Currency> getActiveCurrencies(Integer pageNo, Integer pageSize);

    QueryCrudResponse updateCurrency(Long id, AbsDto absDto);

    QueryCrudResponse setActiveToInverse(Long id);

    QueryCrudResponse deleteCurrency(Long id);

}