package uz.pdp.appwarehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.Input;
import uz.pdp.appwarehouse.payload.InputDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;

import java.time.LocalDate;
import java.util.List;

@Service
public interface InputService {

    QueryCrudResponse addInput(InputDto inputDto);

    Input getInput(Long id);

    Input getByInputCode(String code);

    Page<Input> getByFactureNumber(String factureNumber, Integer pageNo, Integer pageSize);

    Page<Input> getInputsByWarehouse(Long warehouseId, Integer pageNo, Integer pageSize);

    Page<Input> getInputsBySupplier(Long supplierId, Integer pageNo, Integer pageSize);

    Page<Input> getInputsByCurrency(Long currencyId, Integer pageNo, Integer pageSize);

    Page<Input> getInputsBySupplierAndWarehouse(Long supplierId, Long warehouseId, Integer pageNo, Integer pageSize);

    Page<Input> getInputsBySupplierAndCurrency(Long supplierId, Long currencyId, Integer pageNo, Integer pageSize);

    Page<Input> getInputsByWarehouseAndCurrency(Long warehouseId, Long currencyId, Integer pageNo, Integer pageSize);

    List<Input> getInputsBySupplierAndWarehouseAndCurrency(Long supplierId, Long warehouseId, Long currencyId);

    Page<Input> getInputsInBetweenTwoDate(LocalDate start, LocalDate end, Integer pageNo, Integer pageSize);

    QueryCrudResponse updateInput(Long id, InputDto inputDto);

    QueryCrudResponse deleteInput(Long id);

}