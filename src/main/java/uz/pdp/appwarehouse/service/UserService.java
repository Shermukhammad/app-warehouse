package uz.pdp.appwarehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.appwarehouse.entity.User;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.payload.UserDto;
import uz.pdp.appwarehouse.payload.WarehousesSetDto;


@Service
public interface UserService {

    QueryCrudResponse addUser(UserDto userDto);

    QueryCrudResponse addWarehousesToUser(Long id, WarehousesSetDto warehousesDto);

    User getUserById(Long id);

    Page<User> getUsers(Integer pageNo, Integer pageSize);

    Page<User> getActiveUsers(Integer pageNo, Integer pageSize);

    QueryCrudResponse updateUser(Long id, UserDto userDto);

    QueryCrudResponse setActiveToInverse(Long id);

    QueryCrudResponse deleteUser(Long id);

    QueryCrudResponse deleteWarehousesFromUser(Long id, WarehousesSetDto warehousesDto);

}