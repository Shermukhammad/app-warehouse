package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appwarehouse.entity.Input;
import uz.pdp.appwarehouse.payload.InputDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.service.InputService;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/input")
public class InputController {

    private final InputService inputService;

    public InputController(InputService inputService) {
        this.inputService = inputService;
    }


    @GetMapping("/get/{id}")
    public Input getInput(@PathVariable Long id){
        return inputService.getInput(id);
    }


    @GetMapping("/get/factureNumber/{factureNumber}")
    public Page<Input> getByFactureNumber(@PathVariable String factureNumber,
                                          @RequestParam(defaultValue = "0") Integer pageNo,
                                          @RequestParam(defaultValue = "5") Integer pageSize){
        return inputService.getByFactureNumber(factureNumber, pageNo, pageSize);
    }


    @GetMapping("/get/code/{code}")
    public Input getByInputCode(@PathVariable String code){
        return inputService.getByInputCode(code);
    }


    @GetMapping("/get/warehouse/{warehouseId}")
    public Page<Input> getInputsByWarehouse(@PathVariable Long warehouseId,
                                            @RequestParam(defaultValue = "0") Integer pageNo,
                                            @RequestParam(defaultValue = "5") Integer pageSize){
        return inputService.getInputsByWarehouse(warehouseId, pageNo, pageSize);
    }


    @GetMapping("/get/supplier/{supplierId}")
    public Page<Input> getInputsBySupplier(@PathVariable Long supplierId,
                                           @RequestParam(defaultValue = "0") Integer pageNo,
                                           @RequestParam(defaultValue = "5") Integer pageSize){
        return inputService.getInputsBySupplier(supplierId, pageNo, pageSize);
    }


    @GetMapping("/get/currency/{currencyId}")
    public Page<Input> getInputsByCurrency(@PathVariable Long currencyId,
                                           @RequestParam(defaultValue = "0") Integer pageNo,
                                           @RequestParam(defaultValue = "5") Integer pageSize){
        return inputService.getInputsByCurrency(currencyId, pageNo, pageSize);
    }


    @GetMapping("/get/supplier/{supplierId}/warehouse/{warehouseId}")
    public Page<Input> getInputsBySupplierAndWarehouse(@PathVariable("supplierId") Long supplierId,
                                                          @PathVariable("warehouseId") Long warehouseId,
                                                       @RequestParam(defaultValue = "0") Integer pageNo,
                                                       @RequestParam(defaultValue = "5") Integer pageSize){
        return inputService.getInputsBySupplierAndWarehouse(supplierId, warehouseId, pageNo, pageSize);
    }


    @GetMapping("/get/supplier/{supplierId}/currency/{currencyId}")
    public Page<Input> getInputsBySupplierAndCurrency(@PathVariable("supplierId") Long supplierId,
                                                         @PathVariable("currencyId") Long currencyId,
                                                         @RequestParam(defaultValue = "0") Integer pageNo,
                                                         @RequestParam(defaultValue = "5") Integer pageSize){
        return inputService.getInputsBySupplierAndCurrency(supplierId, currencyId, pageNo, pageSize);
    }


    @GetMapping("/get/warehouse/{warehouseId}/currency/{currencyId}")
    public Page<Input> getInputsByWarehouseAndCurrency(@PathVariable("warehouseId") Long warehouseId,
                                                          @PathVariable("currencyId") Long currencyId,
                                                          @RequestParam(defaultValue = "0") Integer pageNo,
                                                          @RequestParam(defaultValue = "5") Integer pageSize){
        return inputService.getInputsByWarehouseAndCurrency(warehouseId, currencyId, pageNo, pageSize);
    }


    @GetMapping("/get/supplier/{supplierId}/warehouse/{warehouseId}/currency/{currencyId}")
    public List<Input> getInputsBySupplierAndWarehouseAndCurrency(
                                                            @PathVariable("supplierId") Long supplierId,
                                                            @PathVariable("warehouseId") Long warehouseId,
                                                            @PathVariable("currencyId") Long currencyId){
        return inputService.getInputsBySupplierAndWarehouseAndCurrency(supplierId, warehouseId, currencyId) ;
    }


    @GetMapping("/get/dateTime/range")
    public Page<Input> getInputsInBetweenTwoDate(@RequestParam("start")
                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                    LocalDate start,
                                                    @RequestParam("end")
                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                    LocalDate end,
                                                    @RequestParam(defaultValue = "0") Integer pageNo,
                                                    @RequestParam(defaultValue = "5") Integer pageSize){
        return inputService.getInputsInBetweenTwoDate(start, end, pageNo, pageSize);
    }


    @PostMapping("/add")
    public QueryCrudResponse addInput(@RequestBody InputDto inputDto){
        return inputService.addInput(inputDto);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateInput(@PathVariable Long id, @RequestBody InputDto inputDto){
        return inputService.updateInput(id, inputDto);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteInput(@PathVariable Long id){
        return inputService.deleteInput(id);
    }

}