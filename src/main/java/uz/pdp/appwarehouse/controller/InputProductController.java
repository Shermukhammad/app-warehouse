package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appwarehouse.entity.InputProduct;
import uz.pdp.appwarehouse.payload.InputProductDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.projections.DailyProductReport;
import uz.pdp.appwarehouse.projections.ProductReportByExpireDate;
import uz.pdp.appwarehouse.service.InputProductService;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/inputProduct")
public class InputProductController {

    private final InputProductService inputProductService;

    public InputProductController(InputProductService inputProductService) {
        this.inputProductService = inputProductService;
    }


    @GetMapping("/get/{id}")
    public InputProduct getById(@PathVariable Long id){
        return inputProductService.getInputProduct(id);
    }


    @GetMapping("/get/input/{inputId}")
    public Page<InputProduct> getByInputId(@PathVariable Long inputId,
                                           @RequestParam(defaultValue = "0") Integer pageNo,
                                           @RequestParam(defaultValue = "10") Integer pageSize){
        return inputProductService.getInputProductsByInput(inputId, pageNo, pageSize);
    }


    @GetMapping("/get/price/range")
    public Page<InputProduct> getByPriceBetween(@RequestParam Double minPrice,
                                                @RequestParam Double maxPrice,
                                                @RequestParam(defaultValue = "0") Integer pageNo,
                                                @RequestParam(defaultValue = "5") Integer pageSize){
        return inputProductService.getInputProductsByPriceBetween(minPrice, maxPrice, pageNo, pageSize);
    }


    @GetMapping("/get/expireDate/today")
    public List<InputProduct> getByExpireDateWithToday(){
        return inputProductService.getInputProductsByExpireDate(LocalDate.now());
    }


    @GetMapping("/count/expireDate/range/next3Days")
    public Long countProductsWithExpireDateRangeInNext3Days(){
        return inputProductService.countProductsWithExpireDateInNext3Days();
    }


    @GetMapping("/get/expireDate/range")
    public Page<InputProduct> getByExpireDateRange(
                                                @RequestParam
                                                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate start,
                                                @RequestParam
                                                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end,
                                                @RequestParam(defaultValue = "0") Integer pageNo,
                                                @RequestParam(defaultValue = "5") Integer pageSize){
        return inputProductService.getInputProductsByExpireDateBetween(start, end, pageNo, pageSize);
    }


    @GetMapping("/get/productReport/today")
    public Page<DailyProductReport> getProductReportToday(@RequestParam(defaultValue = "0") Integer pageNo,
                                                          @RequestParam(defaultValue = "5") Integer pageSize){
        return inputProductService.getProductReportByDate(LocalDate.now(), pageNo, pageSize);
    }


    @GetMapping("/get/productReport/date")
    public Page<DailyProductReport> getProductReportDate(
                                                @RequestParam("date")
                                                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate localDate,
                                                @RequestParam(defaultValue = "0") Integer pageNo,
                                                @RequestParam(defaultValue = "5") Integer pageSize){
        return inputProductService.getProductReportByDate(localDate, pageNo, pageSize);
    }


    @GetMapping("/get/productReport/expireDate/range")
    public Page<ProductReportByExpireDate> getProductReportByExpireDateRange(
                                                @RequestParam
                                                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate start,
                                                @RequestParam
                                                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end,
                                                @RequestParam(defaultValue = "0") Integer pageNo,
                                                @RequestParam(defaultValue = "5") Integer pageSize) {
        return inputProductService.getProductReportByExpireDateRange(start, end, pageNo, pageSize);
    }


    @GetMapping("/get/productReport/expireDate/next3Days")
    public Page<ProductReportByExpireDate> getProductReportByExpireDateRangeInNext3Days(@RequestParam(defaultValue = "0") Integer pageNo,
                                                                                        @RequestParam(defaultValue = "5") Integer pageSize){
        return inputProductService.getProductReportByExpireDateRange(LocalDate.now(), LocalDate.now().plusDays(3), pageNo, pageSize);
    }


    @GetMapping("/get/productReport/expireDate/nextDays/{nextDays}")
    public Page<ProductReportByExpireDate> getProductReportByExpireDateRangeInNextNDays(@PathVariable Long nextDays,
                                                                                        @RequestParam(defaultValue = "0") Integer pageNo,
                                                                                        @RequestParam(defaultValue = "5") Integer pageSize){
        return inputProductService.getProductReportByExpireDateRange(LocalDate.now(), LocalDate.now().plusDays(nextDays), pageNo, pageSize);
    }


    @PostMapping("/add")
    public QueryCrudResponse addInputProduct(@RequestBody InputProductDto inputProductDto){
        return inputProductService.addInputProduct(inputProductDto);
    }


    @PostMapping("/addAll")
    public QueryCrudResponse addAllInputProducts(@RequestBody List<InputProductDto> dtoList){
        return inputProductService.addAllInputProducts(dtoList);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateById(@PathVariable Long id,
                                        @RequestBody InputProductDto inputProductDto){
        return inputProductService.updateInputProduct(id, inputProductDto);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteById(@PathVariable Long id){
        return inputProductService.deleteInputProduct(id);
    }


    @DeleteMapping("/delete/input/{inputId}")
    public QueryCrudResponse deleteAllByInputId(@PathVariable Long inputId){
        return inputProductService.deleteAllByInput(inputId);
    }

}