package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appwarehouse.entity.Client;
import uz.pdp.appwarehouse.payload.ClientDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.service.ClientService;


@RestController
@RequestMapping("/api/client")
public class ClientController {

    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }


    @GetMapping("/get/{id}")
    public Client getClientById(@PathVariable Long id){
        return clientService.getClientById(id);
    }


    @GetMapping("/get/all")
    public Page<Client> getClients(@RequestParam(defaultValue = "0") Integer pageNo,
                                   @RequestParam(defaultValue = "10") Integer pageSize){
        return clientService.getClients(pageNo, pageSize);
    }


    @GetMapping("/get/activeClients")
    public Page<Client> getActiveClients(@RequestParam(defaultValue = "0") Integer pageNo,
                                         @RequestParam(defaultValue = "10") Integer pageSize){
        return clientService.getActiveClients(pageNo, pageSize);
    }


    @PostMapping("/add")
    public QueryCrudResponse addClient(@RequestBody ClientDto clientDto){
        return clientService.addClient(clientDto);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateClient(@PathVariable Long id, @RequestBody ClientDto clientDto){
        return clientService.updateClient(id, clientDto);
    }


    @PutMapping("/update/{id}/inverseActive")
    public QueryCrudResponse setActiveToInverse(@PathVariable Long id){
        return clientService.setActiveToInverse(id);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteClient(@PathVariable Long id){
        return clientService.deleteClient(id);
    }

}