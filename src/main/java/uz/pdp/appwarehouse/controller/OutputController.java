package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appwarehouse.entity.Output;
import uz.pdp.appwarehouse.payload.OutputDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.service.OutputService;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/output")
public class OutputController {

    private final OutputService outputService;

    public OutputController(OutputService outputService) {
        this.outputService = outputService;
    }


    @GetMapping("/get/{id}")
    public Output getOutput(@PathVariable Long id){
        return outputService.getOutput(id);
    }


    @GetMapping("/get/factureNumber/{factureNumber}")
    public Page<Output> getByFactureNumber(@PathVariable String factureNumber,
                                           @RequestParam(defaultValue = "0") Integer pageNo,
                                           @RequestParam(defaultValue = "5") Integer pageSize){
        return outputService.getByFactureNumber(factureNumber, pageNo, pageSize);
    }


    @GetMapping("/get/code/{code}")
    public Output getByOutputCode(@PathVariable String code){
        return outputService.getByOutputCode(code);
    }


    @GetMapping("/get/warehouse/{warehouseId}")
    public Page<Output> getOutputsByWarehouse(@PathVariable Long warehouseId,
                                              @RequestParam(defaultValue = "0") Integer pageNo,
                                              @RequestParam(defaultValue = "5") Integer pageSize){
        return outputService.getOutputsByWarehouse(warehouseId, pageNo, pageSize);
    }


    @GetMapping("/get/client/{clientId}")
    public Page<Output> getOutputsByClient(@PathVariable Long clientId,
                                              @RequestParam(defaultValue = "0") Integer pageNo,
                                              @RequestParam(defaultValue = "5") Integer pageSize){
        return outputService.getOutputsByClient(clientId, pageNo, pageSize);
    }


    @GetMapping("/get/currency/{currencyId}")
    public Page<Output> getOutputsByCurrency(@PathVariable Long currencyId,
                                             @RequestParam(defaultValue = "0") Integer pageNo,
                                             @RequestParam(defaultValue = "5") Integer pageSize){
        return outputService.getOutputsByCurrency(currencyId, pageNo, pageSize);
    }


    @GetMapping("/get/client/{clientId}/warehouse/{warehouseId}")
    public Page<Output> getOutputsByClientAndWarehouse(
                                            @PathVariable("clientId") Long clientId,
                                            @PathVariable("warehouseId") Long warehouseId,
                                            @RequestParam(defaultValue = "0") Integer pageNo,
                                            @RequestParam(defaultValue = "5") Integer pageSize){
        return outputService.getOutputsByClientAndWarehouse(clientId, warehouseId, pageNo, pageSize);
    }


    @GetMapping("/get/client/{clientId}/currency/{currencyId}")
    public Page<Output> getOutputsByClientAndCurrency(
                                            @PathVariable("clientId") Long clientId,
                                            @PathVariable("currencyId") Long currencyId,
                                            @RequestParam(defaultValue = "0") Integer pageNo,
                                            @RequestParam(defaultValue = "5") Integer pageSize){
        return outputService.getOutputsByClientAndCurrency(clientId, currencyId, pageNo, pageSize);
    }


    @GetMapping("/get/warehouse/{warehouseId}/currency/{currencyId}")
    public Page<Output> getOutputsByWarehouseAndCurrency(
                                            @PathVariable("warehouseId") Long warehouseId,
                                            @PathVariable("currencyId") Long currencyId,
                                            @RequestParam(defaultValue = "0") Integer pageNo,
                                            @RequestParam(defaultValue = "5") Integer pageSize){
        return outputService.getOutputsByWarehouseAndCurrency(warehouseId, currencyId, pageNo, pageSize);
    }


    @GetMapping("/get/client/{clientId}/warehouse/{warehouseId}/currency/{currencyId}")
    public List<Output> getOutputsByClientAndWarehouseAndCurrency(
                                            @PathVariable("clientId") Long clientId,
                                            @PathVariable("warehouseId") Long warehouseId,
                                            @PathVariable("currencyId") Long currencyId){
        return outputService.getOutputsByClientAndWarehouseAndCurrency(clientId, warehouseId, currencyId);
    }


    @GetMapping("/get/dateTime/range")
    public Page<Output> getOutputsInBetweenTwoDate(
                                            @RequestParam("start")
                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate start,
                                            @RequestParam("end")
                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end,
                                            @RequestParam(defaultValue = "0") Integer pageNo,
                                            @RequestParam(defaultValue = "5") Integer pageSize){
        return outputService.getOutputsInBetweenTwoDate(start, end, pageNo, pageSize);
    }


    @PostMapping("/add")
    public QueryCrudResponse addOutput(@RequestBody OutputDto outputDto){
        return outputService.addOutput(outputDto);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateOutput(@PathVariable Long id,
                                         @RequestBody OutputDto outputDto){
        return outputService.updateOutput(id, outputDto);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteOutput(@PathVariable Long id){
        return outputService.deleteOutput(id);
    }

}
