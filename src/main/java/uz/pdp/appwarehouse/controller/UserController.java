package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appwarehouse.entity.User;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.payload.UserDto;
import uz.pdp.appwarehouse.payload.WarehousesSetDto;
import uz.pdp.appwarehouse.service.UserService;


@RestController
@RequestMapping("/api/staff")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/get/{id}")
    public User getUserById(@PathVariable Long id){
        return userService.getUserById(id);
    }


    @GetMapping("/get/all")
    public Page<User> getUsers(@RequestParam(defaultValue = "0") Integer pageNo,
                               @RequestParam(defaultValue = "10") Integer pageSize){
        return userService.getUsers(pageNo, pageSize);
    }


    @GetMapping("/get/active")
    public Page<User> getActiveUsers(@RequestParam(defaultValue = "0") Integer pageNo,
                                     @RequestParam(defaultValue = "10") Integer pageSize){
        return userService.getActiveUsers(pageNo, pageSize);
    }


    @PostMapping("/add")
    public QueryCrudResponse addUser(@RequestBody UserDto userDto){
        return userService.addUser(userDto);
    }


    @PostMapping("/{id}/addWarehouses")
    public QueryCrudResponse addWarehousesToUser(@PathVariable Long id, @RequestBody WarehousesSetDto warehousesDto){
        return userService.addWarehousesToUser(id, warehousesDto);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateUser(@PathVariable Long id, @RequestBody UserDto userDto){
        return userService.updateUser(id, userDto);
    }

    @PutMapping("/update/{id}/inverseActive")
    public QueryCrudResponse setActiveToInverse(@PathVariable Long id){
        return userService.setActiveToInverse(id);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteUser(@PathVariable Long id){
        return userService.deleteUser(id);
    }


    @DeleteMapping("/{id}/deleteWarehouses")
    public QueryCrudResponse deleteWarehousesFromUser(@PathVariable Long id, @RequestBody WarehousesSetDto warehousesSetDto){
        return userService.deleteWarehousesFromUser(id, warehousesSetDto);
    }

}