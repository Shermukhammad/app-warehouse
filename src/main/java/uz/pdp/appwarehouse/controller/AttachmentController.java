package uz.pdp.appwarehouse.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.appwarehouse.entity.Attachment;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.service.AttachmentService;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/attachment")
public class AttachmentController {

    private final AttachmentService attachmentService;

    public AttachmentController(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }


    @GetMapping("/get/{id}")
    public Attachment getAttachment(@PathVariable Long id){
        return attachmentService.getAttachmentById(id);
    }


    @GetMapping("/get/download/{id}")
    public QueryCrudResponse downloadAttachment(@PathVariable Long id,
                                                HttpServletResponse response){
        return attachmentService.downloadAttachmentById(id, response);
    }


    @PostMapping(value = "/add" )
    public QueryCrudResponse addAttachment(@RequestPart MultipartFile multipartFile){
        return attachmentService.addAttachment(multipartFile);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateAttachment(@PathVariable Long id, @RequestPart MultipartFile multipartFile){
        return attachmentService.updateAttachment(id, multipartFile);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteAttachment(@PathVariable Long id){
        return attachmentService.deleteAttachmentById(id);
    }

}