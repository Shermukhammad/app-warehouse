package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appwarehouse.entity.Measurement;
import uz.pdp.appwarehouse.payload.AbsDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.service.MeasurementService;


@RestController
@RequestMapping("/api/measurement")
public class MeasurementController {

    private final MeasurementService measurementService;

    public MeasurementController(MeasurementService measurementService) {
        this.measurementService = measurementService;
    }


    @GetMapping("/get/{id}")
    public Measurement getMeasurementById(@PathVariable Long id){
        return measurementService.getMeasurementById(id);
    }


    @GetMapping("/get/all")
    public Page<Measurement> getMeasurements(@RequestParam(defaultValue = "0") Integer pageNo,
                                             @RequestParam(defaultValue = "10") Integer pageSize){
        return measurementService.getMeasurements(pageNo, pageSize);
    }


    @GetMapping("/get/active")
    public Page<Measurement> getActiveMeasurements(@RequestParam(defaultValue = "0") Integer pageNo,
                                                   @RequestParam(defaultValue = "10") Integer pageSize){
        return measurementService.getActiveMeasurements(pageNo, pageSize);
    }


    @PostMapping("/add")
    public QueryCrudResponse addMeasurement(@RequestBody AbsDto absDto){
        return measurementService.addMeasurement(absDto);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateMeasurement(@PathVariable Long id, @RequestBody AbsDto absDto){
        return measurementService.updateMeasurement(id, absDto);
    }


    @PutMapping("/update/{id}/inverseActive")
    public QueryCrudResponse setActiveToInverse(@PathVariable Long id){
        return measurementService.setActiveToInverse(id);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteMeasurement(@PathVariable Long id){
        return measurementService.deleteMeasurement(id);
    }

}