package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appwarehouse.entity.Warehouse;
import uz.pdp.appwarehouse.payload.AbsDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.service.WarehouseService;


@RestController
@RequestMapping("/api/warehouse")
public class WarehouseController {

    private final WarehouseService warehouseService;

    public WarehouseController(WarehouseService warehouseService){
        this.warehouseService = warehouseService;
    }


    @GetMapping("/get/{id}")
    public Warehouse getWarehouseById(@PathVariable Long id){
        return warehouseService.getWarehouseById(id);
    }


    @GetMapping("/get/all")
    public Page<Warehouse> getWarehouses(@RequestParam(defaultValue = "0") Integer pageNo,
                                         @RequestParam(defaultValue = "10") Integer pageSize){
        return warehouseService.getWarehouses(pageNo, pageSize);
    }


    @GetMapping("/get/active")
    public Page<Warehouse> getActiveWarehouses(@RequestParam(defaultValue = "0") Integer pageNo,
                                               @RequestParam(defaultValue = "10") Integer pageSize){
        return warehouseService.getActiveWarehouses(pageNo, pageSize);
    }


    @PostMapping("/add")
    public QueryCrudResponse addWarehouse(@RequestBody AbsDto absDto){
        return warehouseService.addWarehouse(absDto);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateWarehouse(@PathVariable Long id, @RequestBody AbsDto absDto){
        return warehouseService.updateWarehouse(id, absDto);
    }


    @PutMapping("/update/{id}/inverseActive")
    public QueryCrudResponse setActiveToInverse(@PathVariable Long id){
        return warehouseService.setActiveToInverse(id);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteWarehouse(@PathVariable Long id){
        return warehouseService.deleteWarehouse(id);
    }

}