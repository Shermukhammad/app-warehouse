package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appwarehouse.entity.OutputProduct;
import uz.pdp.appwarehouse.payload.OutputProductDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.projections.DailyProductReport;
import uz.pdp.appwarehouse.service.OutputProductService;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/outputProduct")
public class OutputProductController {

    private final OutputProductService outputProductService;

    public OutputProductController(OutputProductService outputProductService) {
        this.outputProductService = outputProductService;
    }


    @GetMapping("/get/{id}")
    public OutputProduct getById(@PathVariable Long id){
        return outputProductService.getOutputProduct(id);
    }


    @GetMapping("/get/output/{outputId}")
    public Page<OutputProduct> getByOutputId(@PathVariable Long outputId,
                                             @RequestParam(defaultValue = "0") Integer pageNo,
                                             @RequestParam(defaultValue = "5") Integer pageSize){
        return outputProductService.getOutputProductsByOutput(outputId, pageNo, pageSize);
    }


    @GetMapping("/get/price/range")
    public Page<OutputProduct> getByPriceBetween(
                                            @RequestParam Double minPrice,
                                            @RequestParam Double maxPrice,
                                            @RequestParam(defaultValue = "0") Integer pageNo,
                                            @RequestParam(defaultValue = "5") Integer pageSize){
        return outputProductService.getOutputProductsByPriceBetween(minPrice, maxPrice, pageNo, pageSize);
    }


    @GetMapping("/get/productReport/today")
    public Page<DailyProductReport> getProductReportToday(@RequestParam(defaultValue = "0") Integer pageNo,
                                                          @RequestParam(defaultValue = "5") Integer pageSize){
        return outputProductService.getProductReportByDate(LocalDate.now(), pageNo, pageSize);
    }


    @GetMapping("/get/productReport/date")
    public Page<DailyProductReport> getProductReportByDate(
                                                        @RequestParam
                                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                        LocalDate localDate,
                                                        @RequestParam(defaultValue = "0") Integer pageNo,
                                                        @RequestParam(defaultValue = "5") Integer pageSize){
        return outputProductService.getProductReportByDate(localDate, pageNo, pageSize);
    }


    @PostMapping("/add")
    public QueryCrudResponse addOutputProduct(@RequestBody OutputProductDto outputProductDto){
        return outputProductService.addOutputProduct(outputProductDto);
    }


    @PostMapping("/addAll")
    public QueryCrudResponse addAllOutputProducts(@RequestBody List<OutputProductDto> dtoList){
        return outputProductService.addAllOutputProducts(dtoList);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateById(@PathVariable Long id, @RequestBody OutputProductDto outputProductDto){
        return outputProductService.updateOutputProduct(id, outputProductDto);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteById(@PathVariable Long id){
        return outputProductService.deleteOutputProduct(id);
    }


    @DeleteMapping("/delete/output/{outputId}")
    public QueryCrudResponse deleteAllByOutputId(@PathVariable Long outputId){
        return outputProductService.deleteAllByOutput(outputId);
    }

}