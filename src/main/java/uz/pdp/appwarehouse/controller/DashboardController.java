package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appwarehouse.entity.InputProduct;
import uz.pdp.appwarehouse.projections.DailyProductReport;
import uz.pdp.appwarehouse.service.InputProductService;
import uz.pdp.appwarehouse.service.OutputProductService;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/dashboard")
public class DashboardController {

    private static final Integer REMAINING_DAYS_TO_EXPIRE_DATE = 2;

    private final InputProductService inputProductService;
    private final OutputProductService outputProductService;


    public DashboardController(InputProductService inputProductService, OutputProductService outputProductService) {
        this.inputProductService = inputProductService;
        this.outputProductService = outputProductService;
    }


    @GetMapping("/getDailyInputProducts/today")
    public Page<DailyProductReport> getDailyInputProducts(@RequestParam(defaultValue = "0") Integer pageNo,
                                                          @RequestParam(defaultValue = "10") Integer pageSize){
        return inputProductService.getProductReportByDate(LocalDate.now(), pageNo, pageSize);
    }


    @GetMapping("/getDailyInputProducts/date")
    public Page<DailyProductReport> getProductReportDate(
                                                        @RequestParam("date")
                                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                                LocalDate localDate,
                                                        @RequestParam(defaultValue = "0")
                                                                Integer pageNo,
                                                        @RequestParam(defaultValue = "5")
                                                                Integer pageSize){
        return inputProductService.getProductReportByDate(localDate, pageNo, pageSize);
    }


    @GetMapping("/getMostDailyOutputProducts/today")
    public Page<DailyProductReport> getProductReportToday(@RequestParam(defaultValue = "0") Integer pageNo,
                                                          @RequestParam(defaultValue = "5") Integer pageSize){
        return outputProductService.getProductReportByDate(LocalDate.now(), pageNo, pageSize);
    }


    @GetMapping("/getMostDailyOutputProducts/date")
    public Page<DailyProductReport> getProductReportByDate(
                                                        @RequestParam
                                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                            LocalDate localDate,
                                                        @RequestParam(defaultValue = "0")
                                                                Integer pageNo,
                                                        @RequestParam(defaultValue = "5")
                                                                Integer pageSize){
        return outputProductService.getProductReportByDate(localDate, pageNo, pageSize);
    }


    @GetMapping("/count/expireDate/range/next3Days")
    public Long countProductsWithExpireDateRangeInNext3Days(){
        return inputProductService.countProductsWithExpireDateInNext3Days();
    }


    @GetMapping("/getInputProductsComingExpireDate")
    public Page<InputProduct> getInputProductsComingExpireDate(
                                        @RequestParam(defaultValue = "0") Integer pageNo,
                                        @RequestParam(defaultValue = "5") Integer pageSize){
        return inputProductService.getInputProductsComingExpireDate(LocalDate.now(), pageNo, pageSize);
    }


    @GetMapping("/notification/byExpireDateInNextGivenDays")
    public Page<InputProduct> getByExpireDateRange(
                                        @RequestParam(defaultValue = "0")
                                                Integer pageNo,
                                        @RequestParam(defaultValue = "5")
                                                Integer pageSize){
        return inputProductService.getInputProductsByExpireDateBetween(
                LocalDate.now(),
                LocalDate.now().plusDays(REMAINING_DAYS_TO_EXPIRE_DATE),
                pageNo, pageSize);
    }

}
