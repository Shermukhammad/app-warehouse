    package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appwarehouse.entity.Currency;
import uz.pdp.appwarehouse.payload.AbsDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.service.CurrencyService;


@RestController
@RequestMapping("/api/currency")
public class CurrencyController {

    private final CurrencyService currencyService;

    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }


    @GetMapping("/get/{id}")
    public Currency getCurrencyById(@PathVariable Long id){
        return currencyService.getCurrencyById(id);
    }


    @GetMapping("/get/all")
    public Page<Currency> getCurrenciesList(@RequestParam(defaultValue = "0") Integer pageNo,
                                            @RequestParam(defaultValue = "10") Integer pageSize){
        return currencyService.getCurrencies(pageNo, pageSize);
    }


    @GetMapping("/get/activeCurrencies")
    public Page<Currency> getActiveCurrencies(@RequestParam(defaultValue = "0") Integer pageNo,
                                              @RequestParam(defaultValue = "10") Integer pageSize){
        return currencyService.getActiveCurrencies(pageNo, pageSize);
    }


    @PostMapping("/add")
    public QueryCrudResponse addCurrency(@RequestBody AbsDto absDto){
        return currencyService.addCurrency(absDto);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateCurrency(@PathVariable Long id, @RequestBody AbsDto absDto){
        return currencyService.updateCurrency(id, absDto);
    }


    @PutMapping("/update/{id}/inverseActive")
    public QueryCrudResponse setActiveToInverse(@PathVariable Long id){
        return currencyService.setActiveToInverse(id);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteCurrency(@PathVariable Long id){
        return currencyService.deleteCurrency(id);
    }

}