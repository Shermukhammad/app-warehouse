package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appwarehouse.entity.Supplier;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.payload.SupplierDto;
import uz.pdp.appwarehouse.service.SupplierService;


@RestController
@RequestMapping("/api/supplier")
public class SupplierController {

    private final SupplierService supplierService;

    public SupplierController(SupplierService supplierService) {
        this.supplierService = supplierService;
    }


    @GetMapping("/get/{id}")
    public Supplier getSupplierById(@PathVariable Long id){
        return supplierService.getSupplierById(id);
    }


    @GetMapping("/get/all")
    public Page<Supplier> getSuppliers(@RequestParam(defaultValue = "0") Integer pageNo,
                                       @RequestParam(defaultValue = "10") Integer pageSize){
        return supplierService.getSuppliers(pageNo, pageSize);
    }


    @GetMapping("/get/active")
    public Page<Supplier> getActiveSuppliers(@RequestParam(defaultValue = "0") Integer pageNo,
                                                @RequestParam(defaultValue = "10") Integer pageSize){
        return supplierService.getActiveSuppliers(pageNo, pageSize);
    }


    @PostMapping("/add")
    public QueryCrudResponse addSupplier(@RequestBody SupplierDto supplierDto){
        return supplierService.addSupplier(supplierDto);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateSupplier(@PathVariable Long id, @RequestBody SupplierDto supplierDto){
        return supplierService.updateSupplier(id, supplierDto);
    }


    @PutMapping("/update/{id}/inverseActive")
    public QueryCrudResponse setActiveToInverse(@PathVariable Long id){
        return supplierService.setActiveToInverse(id);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteSupplier(@PathVariable Long id){
        return supplierService.deleteSupplier(id);
    }

}