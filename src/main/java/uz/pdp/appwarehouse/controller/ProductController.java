package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.appwarehouse.entity.Product;
import uz.pdp.appwarehouse.payload.ProductDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.service.ProductService;

import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @GetMapping("/get/{id}")
    public Product getProductById(@PathVariable Long id){
        return productService.getProductById(id);
    }


    @GetMapping("/get/all")
    public Page<Product> getProducts(@RequestParam(defaultValue = "0") Integer pageNo,
                                     @RequestParam(defaultValue = "10") Integer pageSize){
        return productService.getProducts(pageNo, pageSize);
    }


    @GetMapping("/get/category/{categoryId}")
    public List<Product> getProductsByCategoryId(@PathVariable Long categoryId){
        return productService.getProductsByCategoryId(categoryId);
    }


    @GetMapping("/get/measurement/{measurementId}")
    public List<Product> getProductsByMeasurementById(@PathVariable Long measurementId){
        return productService.getProductsByMeasurementId(measurementId);
    }


    @GetMapping("/get/active")
    public Page<Product> getActiveProducts(@RequestParam(defaultValue = "0") Integer pageNo,
                                           @RequestParam(defaultValue = "10") Integer pageSize){
        return productService.getActiveProducts(pageNo, pageSize);
    }


    @GetMapping("/get/category/{categoryId}/active")
    public List<Product> getActiveProductsByCategoryId(@PathVariable Long categoryId){
        return productService.getActiveProductsByCategoryId(categoryId);
    }


    @GetMapping("/get/measurement/{measurementId}/active")
    public List<Product> getActiveProductsByMeasurementId(@PathVariable Long measurementId){
        return productService.getActiveProductsByMeasurementId(measurementId);
    }


    @PostMapping("/add")
    public QueryCrudResponse addProduct(@RequestParam("model") String model, @RequestParam(value = "file", required = false) MultipartFile file){
        return productService.addProduct(model, file);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateProduct(@PathVariable Long id, @RequestBody ProductDto productDto){
        return productService.updateProduct(id, productDto);
    }


    @PutMapping("/update/{productId}/changeToCategory/{categoryId}")
    public QueryCrudResponse changeCategoryOfProductById(@PathVariable Long productId, @PathVariable Long categoryId){
        return productService.changeProductCategory(productId, categoryId);
    }


    @PutMapping("/update/{id}/inverseActive")
    public QueryCrudResponse setActiveToInverse(@PathVariable Long id){
        return productService.setActiveToInverse(id);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteProductById(@PathVariable Long id){
        return productService.deleteProductById(id);
    }

}