package uz.pdp.appwarehouse.controller;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appwarehouse.entity.Category;
import uz.pdp.appwarehouse.payload.CategoryDto;
import uz.pdp.appwarehouse.payload.QueryCrudResponse;
import uz.pdp.appwarehouse.service.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @GetMapping("/get/{id}")
    public Category getCategoryById(@PathVariable Long id){
        return categoryService.getCategoryById(id);
    }


    @GetMapping("/get/all")
    public Page<Category> getCategories(@RequestParam(defaultValue = "0") Integer pageNo,
                                        @RequestParam(defaultValue = "10") Integer pageSize){
        return categoryService.getCategories(pageNo, pageSize);
    }


    @GetMapping("/superParentCategories/get/all")
    public List<Category> getSuperParentCategories(){
        return categoryService.getSuperParentCategories();
    }


    @GetMapping("/superParentCategories/get/active")
    public List<Category> getActiveSuperParentCategories(){
        return categoryService.getActiveSuperParentCategories();
    }


    @GetMapping("/get/{parentCategoryId}/childrenCategories")
    public List<Category> getChildrenCategories(@PathVariable Long parentCategoryId){
        return categoryService.getChildrenCategories(parentCategoryId);
    }


    @GetMapping("/get/{parentCategoryId}/childrenCategories/active")
    public List<Category> getActiveChildrenCategories(@PathVariable Long parentCategoryId){
        return categoryService.getActiveChildrenCategories(parentCategoryId);
    }


    @GetMapping("/get/active")
    public Page<Category> getActiveCategories(@RequestParam(defaultValue = "0") Integer pageNo,
                                              @RequestParam(defaultValue = "10") Integer pageSize){
        return categoryService.getActiveCategories(pageNo, pageSize);
    }


    @PostMapping("/add")
    public QueryCrudResponse addCategory(@RequestBody CategoryDto categoryDto){
        return categoryService.addCategory(categoryDto);
    }


    @PutMapping("/update/{id}")
    public QueryCrudResponse updateCategory(@PathVariable Long id, @RequestBody CategoryDto categoryDto){
        return categoryService.updateCategory(id, categoryDto);
    }


    @PutMapping("/update/{id}/inverseActive")
    public QueryCrudResponse setActiveToInverse(@PathVariable Long id){
        return categoryService.setActiveToInverse(id);
    }


    @DeleteMapping("/delete/{id}")
    public QueryCrudResponse deleteCategory(@PathVariable Long id){
        return categoryService.deleteCategory(id);
    }

}