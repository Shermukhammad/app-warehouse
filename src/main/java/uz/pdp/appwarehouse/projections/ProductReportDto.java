package uz.pdp.appwarehouse.projections;

import java.io.Serializable;


public class ProductReportDto implements Serializable {
    private String name;
    private String productCode;
    private String measurementName;
    private Double totalAmount;
    private Double price;
}