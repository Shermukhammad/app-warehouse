package uz.pdp.appwarehouse.projections;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface ProductReportByExpireDate {
    String getProduct();
    Double getAmount();
    String getMeasurement();
    Double getPrice();
    String getCurrency();
    String getWarehouse();
    String getSupplier();
    LocalDateTime getAcceptedDateTime();
    LocalDate getExpireDate();
}
