package uz.pdp.appwarehouse.projections;

public interface DailyProductReport {
    String getProductName();
    String getProductCode();
    Double getTotalAmount();
    String getMeasurementName();
    Double getPrice();
    Double getTotalPrice();
}
